import { Handler } from "aws-lambda";
import "reflect-metadata";
import { container } from "tsyringe";
import "./diConfig";
import { AddNormalUserToNamespace } from "./operations/AddNormalUserToNamespace";
import { CreateOrUpdateCokeBattle } from "./operations/CreateOrUpdateCokeBattle";
import { CreateOrUpdateCokeBattlesUser } from "./operations/CreateOrUpdateCokeBattlesUser";
import { CreateOrUpdateNamespace } from "./operations/CreateOrUpdateNamespace";
import { DeleteCokeBattle } from "./operations/DeleteCokeBattle";
import { GetNamespace } from "./operations/GetNamespace";
import { GetUserProfileFromAuth0ByToken } from "./operations/GetUserProfileFromAuth0ByToken";
import { ListCokeBattles } from "./operations/ListCokeBattles";
import { ListCokeBattlesUsers } from "./operations/ListCokeBattlesUsers";
import { ListNamespaceUsers } from "./operations/ListNamespaceUsers";
import { ListUserNamespaces } from "./operations/ListUserNamespaces";
import { RemoveUserFromNamespace } from "./operations/RemoveUserFromNamespace";
import { errorToHttpStatusConvertingHandler, queryMappers, validatingQueryHandler } from "./utils/handlerUtils";

export const addNormalUserToNamespace: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        AddNormalUserToNamespace.prototype.handle.bind(container.resolve(AddNormalUserToNamespace)), {
        namespaceId: queryMappers.require,
        userId: queryMappers.require
    })));

export const createOrUpdateCokeBattle: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        CreateOrUpdateCokeBattle.prototype.createOrUpdateCokeBattle.bind(container.resolve(CreateOrUpdateCokeBattle)), {
    })));

export const createOrUpdateCokeBattlesUser: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        CreateOrUpdateCokeBattlesUser.prototype.createOrUpdateCokeBattlesUser.bind(container.resolve(CreateOrUpdateCokeBattlesUser)), {
    })));

export const createOrUpdateNamespace: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        CreateOrUpdateNamespace.prototype.createOrUpdateNamespace.bind(container.resolve(CreateOrUpdateNamespace)), {
    })));

export const deleteCokeBattle: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        DeleteCokeBattle.prototype.deleteCokeBattle.bind(container.resolve(DeleteCokeBattle)), {
    })));


export const getNamespace: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        GetNamespace.prototype.getNamespace.bind(container.resolve(GetNamespace)), {
        namespaceId: queryMappers.require
    })));

export const getUserProfileFromAuth0: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        GetUserProfileFromAuth0ByToken.prototype.getProfile.bind(container.resolve(GetUserProfileFromAuth0ByToken)), {
        accessToken: queryMappers.require
    })));

export const listCokeBattles: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        ListCokeBattles.prototype.listCokeBattles.bind(container.resolve(ListCokeBattles)), {
        namespaceId: queryMappers.require
    })));


export const listCokeBattlesUsers: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        ListCokeBattlesUsers.prototype.listCokeBattlesUsers.bind(container.resolve(ListCokeBattlesUsers)), {
        searchString: queryMappers.pass
    })));


export const removeUserFromNamespace: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        RemoveUserFromNamespace.prototype.removeUserFromNamespace.bind(container.resolve(RemoveUserFromNamespace)), {
        namespaceId: queryMappers.require,
        userId: queryMappers.require
    })));

export const listUserNamespaces: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        ListUserNamespaces.prototype.listUserNamespaces.bind(container.resolve(ListUserNamespaces)), {
        userId: queryMappers.require
    })));

export const listNamespaceUsers: Handler = lazy(() => errorToHttpStatusConvertingHandler(
    validatingQueryHandler(
        ListNamespaceUsers.prototype.listNamespaceUsers.bind(container.resolve(ListNamespaceUsers)), {
        namespaceId: queryMappers.require
    })));


function lazy(provider: () => Handler): Handler {
    let handler: Handler | null = null;
    return (event, ctx, callback) => {
        if (handler == null) {
            handler = provider();
        }
        return handler(event, ctx, callback);
    }
}