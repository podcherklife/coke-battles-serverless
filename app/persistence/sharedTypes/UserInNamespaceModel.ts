import * as zod from "zod";
import { NamespaceRefSchema } from "./Namespace";
import { BaseUserSchema, UserRefSchema } from "./User";

export const UserRoleInNamespaceSchema = zod.literal("admin").or(zod.literal("participant"));
export type UserInNamespaceRole = zod.infer<typeof UserRoleInNamespaceSchema>

export const UserParticipationInNamespaceSchema = zod.object({
    namespace: NamespaceRefSchema,
    user: UserRefSchema,
    role: UserRoleInNamespaceSchema,
    displayName: BaseUserSchema.shape.username
});
export type UserParticipationInNamespace = zod.infer<typeof UserParticipationInNamespaceSchema>;
