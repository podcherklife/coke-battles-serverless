import * as zod from "zod";
import { NamespaceRefSchema } from "./Namespace";
import { BaseUserSchema, UserRefSchema } from "./User";

export const SimpleUserSchema = BaseUserSchema.pick({ id: true, username: true });
export type SimpleUser = zod.infer<typeof SimpleUserSchema>;

export const ParticipationSideMinimalSchema = zod.object({
    description: zod.string().nullable(),
    participants: zod.array(UserRefSchema)
});
export type ParticipationSideMinimal = zod.infer<typeof ParticipationSideMinimalSchema>;

export const ParticipationSideRichSchema = ParticipationSideMinimalSchema.extend({
    participants: zod.array(SimpleUserSchema)
})
//FIXME: these schema is not part of persistence
export type ParticipationSideRich = zod.infer<typeof ParticipationSideRichSchema>;


export const CokeBattleIdSchema = zod.string();

export const CokeBattleSchema = zod.object({
    id: CokeBattleIdSchema,
    creationDate: zod.number(),
    creator: UserRefSchema,
    description: zod.string(),
    expectedResolutionDate: zod.number().nullable(),
    sides: zod.array(ParticipationSideMinimalSchema),
    namespace: NamespaceRefSchema
});
export type CokeBattle = zod.infer<typeof CokeBattleSchema>;