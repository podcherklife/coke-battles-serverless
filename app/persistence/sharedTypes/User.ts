import * as zod from "zod";

export const UserTypeSchema = zod.literal("dummy").or(zod.literal("bot")).or(zod.literal("human"));
export type UserType = zod.infer<typeof UserTypeSchema>;

export const BaseUserSchema = zod.object({
    id: zod.string(),
    username: zod.string(),
    email: zod.string().nullable(),
    type: UserTypeSchema,
});
export type BaseUser = zod.infer<typeof BaseUserSchema>;

export const UserIdSchema = BaseUserSchema.shape.id;
export type UserId = zod.infer<typeof UserIdSchema>;

export const RegisteredUserSchema = BaseUserSchema.extend({
    email: zod.string()
});
export type RegisteredUser = zod.infer<typeof RegisteredUserSchema>;

export const UserRefSchema = BaseUserSchema.pick({ id: true });
export type UserRef = zod.infer<typeof UserRefSchema>;
