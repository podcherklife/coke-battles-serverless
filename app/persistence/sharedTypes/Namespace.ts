import * as zod from "zod";
import { UserRefSchema } from "./User";


export const NamespaceSchema = zod.object({
    name: zod.string(),
    id: zod.string(),
    creator: UserRefSchema,
});
export type Namespace = zod.infer<typeof NamespaceSchema>;

export const NamespaceRefSchema = NamespaceSchema.pick({
    id: true,
});
export type NamespaceRef = zod.infer<typeof NamespaceRefSchema>;

export const NamespaceIdSchema = NamespaceSchema.shape.id;
export type NamespaceId = zod.infer<typeof NamespaceIdSchema>;

