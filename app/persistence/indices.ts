export type PK = {
    pk: string,
    sk: string
}

export type EntityType = {
    entityType: string
}

export type Gsi1 = {
    pk1: string,
    sk1: string
}

export type Lsi1 = {
    lsi1: string
}

export type Lsi2 = {
    lsi2: string
}

export const Lsi1Name = "pk-lsi1-index";
export const Lsi2Name = "pk-lsi2-index";
export const Gsi1Name = "pk1-sk1-index";