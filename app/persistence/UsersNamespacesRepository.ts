import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { inject, injectable } from "tsyringe";
import * as zod from "zod";
import { AbstractRepository } from "./AbstractRepository";
import { DynamoDbProperties, DynamoDbPropertiesInjectionToken } from "./DynamoDbProperties";
import { Gsi1, Gsi1Name, Lsi1, PK } from "./indices";
import { NamespaceRef } from "./sharedTypes/Namespace";
import { UserRef } from "./sharedTypes/User";
import { UserParticipationInNamespace, UserParticipationInNamespaceSchema } from "./sharedTypes/UserInNamespaceModel";

export const userInNamespaceEntityName = "user_in_namespace";

type UserParticipationId = {
    namespaceId: UserParticipationInNamespace['namespace']['id']
    userId: UserParticipationInNamespace['user']['id']
}

@injectable()
export class UsersNamespacesRepository extends AbstractRepository<UserParticipationInNamespace> {
    usersTableName: string;
    namespacesTableName: string;

    constructor(dynamoDb: DynamoDBDocument, @inject(DynamoDbPropertiesInjectionToken) dynamoDbProperties: DynamoDbProperties) {
        super(userInNamespaceEntityName, dynamoDb, dynamoDbProperties.mainTableName);
        this.dynamoDb = dynamoDb;
        this.usersTableName = dynamoDbProperties.mainTableName;
        this.namespacesTableName = dynamoDbProperties.mainTableName;
    }

    public getUserNamespaces(user: UserRef): Promise<UserParticipationInNamespace[]> {
        return this.dynamoDb.query({
            IndexName: Gsi1Name,
            TableName: this.tableName,
            KeyConditionExpression: `${this.gsi1PkProp} = :user_pk1`,
            ExpressionAttributeValues: {
                ":user_pk1": `${this.gsi1PkPrefix}${user.id}`
            },
            Limit: 10
        }).then(result => zod.array(UserParticipationInNamespaceSchema).parse(result.Items));
    }

    public getNamespaceUsers(namespace: NamespaceRef): Promise<UserParticipationInNamespace[]> {
        return this.dynamoDb.query({
            TableName: this.tableName,
            KeyConditionExpression: `${this.pkProp} = :user_namespace_pk_value and begins_with(${this.skProp}, :sk_prefix_value)`,
            ExpressionAttributeValues: {
                ":user_namespace_pk_value": `${this.pkPrefix}${namespace.id}`,
                ":sk_prefix_value": this.skPrefix
            },
            Limit: 10
        }).then(result => zod.array(UserParticipationInNamespaceSchema).parse(result.Items));
    }

    public searchNamespaceUsersByName(namespace: NamespaceRef, searchString: string): Promise<UserParticipationInNamespace[]> {
        return this.dynamoDb.query({
            TableName: this.tableName,
            KeyConditionExpression: `${this.pkProp} = :user_namespace_pk_value and begins_with(${this.lsi1Prop}, :lsi1_prefix_value)`,
            ExpressionAttributeValues: {
                ":user_namespace_pk_value": `${this.pkPrefix}${namespace.id}`,
                ":lsi1_prefix_value": `${this.lsi1Prefix}${searchString}`,
            },
            Limit: 10
        }).then(result => zod.array(UserParticipationInNamespaceSchema).parse(result.Items));
    }

    public saveUserInNamespace(item: UserParticipationInNamespace) {
        return super.save(item);
    }

    public deleteUserFromNamespace(key: UserParticipationId) {
        return super.delete(this.generatePkProperties(key));
    }

    public getUserInNamespace(key: UserParticipationId): Promise<UserParticipationInNamespace | null> {
        return super.get(this.generatePkProperties(key));
    }

    protected generatePkProperties(item: UserParticipationInNamespace | UserParticipationId): PK {
        return {
            pk: `${this.pkPrefix}${"user" in item && "namespace" in item ? item.namespace.id : item.namespaceId}`,
            sk: `${this.skPrefix}${"user" in item && "namespace" in item ? item.user.id : item.userId}`,
        };
    }

    protected generateGsi1Properties(item: UserParticipationInNamespace): Gsi1 {
        return {
            pk1: `${this.gsi1PkPrefix}${item.user.id}`,
            sk1: `${this.gsi1SkPrefix}${item.namespace.id}`
        };
    }

    protected generateLsi1Properties(item: UserParticipationInNamespace): Lsi1 {
        return {
            lsi1: `${this.lsi1Prefix}${item.displayName}`
        }
    }

    protected stripProperties(item: UserParticipationInNamespace): UserParticipationInNamespace {
        return UserParticipationInNamespaceSchema.parse(item);
    }

    private pkPrefix = "namespace#";
    private skPrefix = "user#";
    private gsi1PkPrefix = "user_in_namespace_by_user_id#";
    private gsi1SkPrefix = "namespace#";
    private lsi1Prefix = "user_in_namespace_by_name#"

}
