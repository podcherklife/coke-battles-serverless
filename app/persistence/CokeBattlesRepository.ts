import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { inject, injectable } from "tsyringe";
import * as zod from "zod";
import { DynamoDbProperties, DynamoDbPropertiesInjectionToken } from "./DynamoDbProperties";
import { UsersRepository } from "./UsersRepository";
import { EntityType, Lsi1, Lsi2, PK } from "./indices";
import { CokeBattle, CokeBattleSchema } from "./sharedTypes/CokeBattle";
import { NamespaceRef } from "./sharedTypes/Namespace";


type SavedBattle = CokeBattle & PK & Lsi1 & Lsi2 & EntityType;


type BattleId = CokeBattle['id'];
type NamespaceId = CokeBattle['namespace']['id'];

@injectable()
export class CokeBattlesRepository {
    dynamoDb: DynamoDBDocument;
    usersRepository: UsersRepository;
    tableName: string;

    constructor(
        usersRepository: UsersRepository,
        dynamoDb: DynamoDBDocument,
        @inject(DynamoDbPropertiesInjectionToken) dynamoDbProperties: DynamoDbProperties) {
        this.dynamoDb = dynamoDb;
        this.usersRepository = usersRepository;
        this.tableName = dynamoDbProperties.mainTableName;
    }

    async listBattles(namespaceId: NamespaceRef['id']): Promise<CokeBattle[]> {
        const queryResult = await this.dynamoDb.query({
            TableName: this.tableName,
            Limit: 10,
            KeyConditionExpression: "pk = :namespace and begins_with(sk, :battle_sk_prefix)",
            ExpressionAttributeValues: {
                ":namespace": `namespace#${namespaceId}`,
                ":battle_sk_prefix": `battle#`
            }
        });

        return zod.array(CokeBattleSchema).parse(queryResult.Items || []);
    }

    async getBattleById(namespaceId: NamespaceId, battleId: BattleId): Promise<CokeBattle | null> {
        let getResult = await this.dynamoDb.get({
            TableName: this.tableName,
            Key: this.generatePkProperties(namespaceId, battleId)
        });
        const item = CokeBattleSchema.optional().parse(getResult.Item);
        return item || null;
    }

    async saveBattle(battle: CokeBattle): Promise<void> {
        return this.dynamoDb.transactWrite({
            TransactItems: [
                {
                    ConditionCheck: {
                        TableName: this.tableName,
                        Key: {
                            pk: "namespace",
                            sk: `namespace#${battle.namespace.id}`
                        } as PK,
                        ConditionExpression: "attribute_exists(pk)"
                    },
                },
                {
                    Put: {
                        TableName: this.tableName,
                        Item: this.generateSavedBattle(CokeBattleSchema.parse(battle))
                    }
                }
            ]
        }).then(() => void 0);
    }

    async deleteBattle(namespaceId: NamespaceId, battleId: BattleId): Promise<void> {
        return this.dynamoDb.delete({
            TableName: this.tableName,
            Key: this.generatePkProperties(namespaceId, battleId)
        }).then(_ => Promise.resolve());
    }

    private generateSavedBattle(battle: CokeBattle): SavedBattle {
        return {
            ...battle,
            ...this.generateLsi1Properties(battle),
            ...this.generateLsi2Properties(battle),
            ...this.generatePkProperties(battle.namespace.id, battle.id),
            entityType: "battle"
        };
    }

    private generatePkProperties(namespaceId: NamespaceId, battleId: BattleId): PK {
        return {
            pk: `namespace#${namespaceId}`,
            sk: `battle#${battleId}`
        };
    }

    private generateLsi1Properties(battle: CokeBattle): Lsi1 {
        return {
            lsi1: `battle_by_creation_date#${battle.creationDate}`
        };
    }

    private generateLsi2Properties(battle: CokeBattle): Lsi2 {
        return {
            lsi2: `battle_by_resolution_date#${battle.expectedResolutionDate}`
        }
    }

}
