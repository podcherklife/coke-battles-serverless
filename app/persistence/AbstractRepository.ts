import { BatchStatementErrorCodeEnum, TransactionCanceledException } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument, TransactWriteCommandInput } from "@aws-sdk/lib-dynamodb";
import { EntityType, Gsi1, Lsi1, Lsi2, PK } from "./indices";


type Indicies = PK & (Lsi1 | {}) & (Lsi2 | {}) & (Gsi1 | {});
export type UniquenessItem = EntityType & { key: PK, tableName: string };

export class ConditionCheckError extends Error {
    constructor(message: string) {
        super(message);
    }
}

export abstract class AbstractRepository<TEntity> {
    entityType: string;
    dynamoDb: DynamoDBDocument;
    tableName: string


    constructor(entityType: string, dynamoDb: DynamoDBDocument, tableName: string) {
        this.entityType = entityType;
        this.dynamoDb = dynamoDb;
        this.tableName = tableName;
    }

    public async applyChange(items: { added: TEntity[], modified: TEntity[], removed: TEntity[] }): Promise<void> {
        return this.applyActions(await this.generateActions(items));
    }

    public async generateActions(items: { added: TEntity[], modified: TEntity[], removed: TEntity[] }): Promise<Exclude<TransactWriteCommandInput['TransactItems'], undefined>> {
        let promises = [
            ...items.removed.map(e => this.generateDeleteActions(e)),
            ...items.added.concat(items.modified).map(e => this.generateWriteActions(e))
        ]
        return Promise.all(promises).then(v => v.reduce((a, e) => a.concat(e), []))
    }

    public async applyActions(actions: Exclude<TransactWriteCommandInput['TransactItems'], undefined>): Promise<void> {
        this.dynamoDb.transactWrite({ TransactItems: actions }).catch((e: any) => {
            if ('CancellationReasons' in e) {
                const transactionCancelledException = e as TransactionCanceledException;
                const reasons = transactionCancelledException.CancellationReasons || [];
                if (reasons.find(e => e.Code == BatchStatementErrorCodeEnum.ConditionalCheckFailed)) {
                    throw new ConditionCheckError(e.message);
                } else {
                    throw e;
                }
            } else {
                throw e;
            }
        }).then(_ => void 0);
    }

    protected async generateDeleteActions(item: TEntity): Promise<Exclude<TransactWriteCommandInput['TransactItems'], undefined>> {
        let pk = this.generatePkProperties(item)
        let currentState = await this.get(pk);
        if (currentState != null) {
            return [
                {
                    Delete: {
                        TableName: this.tableName,
                        Key: pk
                    },
                },
                ...this.generateUniquenessItems(currentState).map(e => ({
                    Delete: {
                        TableName: e.tableName,
                        Key: e.key
                    },
                }))
            ]
        } else {
            throw new Error("trying to delete item that does not actually exist")
        }
    }

    protected async generateWriteActions(item: TEntity): Promise<Exclude<TransactWriteCommandInput['TransactItems'], undefined>> {
        let currentState = await this.get(this.generatePkProperties(item));
        let existingUniquenessItems = currentState == null ? [] : this.generateUniquenessItems(currentState);
        let desiredUniquenessItems = this.generateUniquenessItems(item);
        let knownKeys = new Set<string>();
        let unchangedItemKeys = new Set<string>();

        let generateUniquenessItemKey = (item: UniquenessItem) => `${item.key.pk}|${item.key.sk}`;

        for (let uniquenessItem of existingUniquenessItems.concat(desiredUniquenessItems)
            .map(e => ({ str: generateUniquenessItemKey(e), item: e }))) {

            if (knownKeys.has(uniquenessItem.str)) {
                unchangedItemKeys.add(uniquenessItem.str);
            } else {
                knownKeys.add(uniquenessItem.str);
            }
        }

        return [
            {
                Put: {
                    TableName: this.tableName,
                    Item: this.generateIndexedItem(this.stripProperties(item)),
                    ConditionExpression: currentState == null ? "attribute_not_exists(pk)" : "attribute_exists(pk)"
                },
            },
            ...desiredUniquenessItems.filter(uniquenessItem => !unchangedItemKeys.has(generateUniquenessItemKey(uniquenessItem))).map(e => ({
                Put: {
                    TableName: e.tableName,
                    Item: {
                        ...e.key,
                        entityName: e.entityType
                    },
                    ConditionExpression: "attribute_not_exists(pk)"
                },
            })),
            ...existingUniquenessItems.filter(uniquenessItem => !unchangedItemKeys.has(generateUniquenessItemKey(uniquenessItem))).map(e => ({
                Delete: {
                    TableName: e.tableName,
                    Key: e.key,
                    ConditionExpression: "attribute_exists(pk)"
                },
            }))
        ]
    }

    protected async save(item: TEntity): Promise<void> {
        let commands = { TransactItems: await this.generateActions({ added: [item], removed: [], modified: [] }) }
        return this.dynamoDb.transactWrite(commands)
            .catch((e: any) => {
                if ('CancellationReasons' in e) {
                    const transactionCancelledException = e as TransactionCanceledException;
                    const reasons = transactionCancelledException.CancellationReasons || [];
                    if (reasons.find(e => e.Code == BatchStatementErrorCodeEnum.ConditionalCheckFailed)) {
                        throw new ConditionCheckError(e.message);
                    } else {
                        throw e;
                    }
                } else {
                    throw e;
                }
            }).then(_ => void 0);
    }

    protected async get(pk: PK): Promise<TEntity | null> {
        return this.dynamoDb.get({
            TableName: this.tableName,
            Key: pk
        }).then(result => result.Item ? this.stripProperties(result.Item as TEntity) : null);
    }

    protected async delete(pk: PK): Promise<void> {
        let currentState = await this.get(pk);
        if (currentState != null) {
            return this.dynamoDb.transactWrite({
                TransactItems: [
                    {
                        Delete: {
                            TableName: this.tableName,
                            Key: pk
                        },
                    },
                    ...this.generateUniquenessItems(currentState).map(e => ({
                        Delete: {
                            TableName: e.tableName,
                            Key: e.key
                        },
                    }))
                ]
            }).then(_ => void 0);
        } else {
            return Promise.resolve();
        }
    }

    protected generateIndexedItem(item: TEntity): TEntity & Indicies & EntityType {
        return {
            ...item,
            ...this.generatePkProperties(item),
            ...this.generateLsi1Properties(item),
            ...this.generateGsi1Properties(item),
            entityType: this.entityType
        };
    }

    protected generateUniquenessItems(_: TEntity): UniquenessItem[] {
        return [];
    }

    protected lsi1(lsi1Val: Lsi1['lsi1']): Lsi1 {
        return {
            [this.lsi1Prop]: lsi1Val,
        }
    }

    protected lsi2(lsi2Val: Lsi2['lsi2']): Lsi2 {
        return {
            [this.lsi2Prop]: lsi2Val
        }
    }

    protected lsi1Prop: keyof Lsi1 = "lsi1";
    protected lsi2Prop: keyof Lsi2 = "lsi2";
    protected pkProp: keyof Pick<PK, "pk"> = "pk";
    protected skProp: keyof Pick<PK, "sk"> = "sk";
    protected gsi1PkProp: keyof Pick<Gsi1, "pk1"> = "pk1";
    protected gsi1SkProp: keyof Pick<Gsi1, "sk1"> = "sk1";


    protected abstract generatePkProperties(item: TEntity): PK;
    protected abstract stripProperties(_item: TEntity): TEntity;
    protected generateLsi1Properties(_item: TEntity): Lsi1 | {} { return {}; }
    protected generateLsi2Properties(_item: TEntity): Lsi2 | {} { return {}; }
    protected generateGsi1Properties(_item: TEntity): Gsi1 | {} { return {}; }

}