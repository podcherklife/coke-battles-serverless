import { injectable } from "tsyringe";
import { UserManagement } from "../context/RoleManagenent";
import { Namespace, NamespaceRef } from "./sharedTypes/Namespace";
import { UserRef } from "./sharedTypes/User";
import { NamespacesRepository } from "./NamespacesRepository";
import { UsersNamespacesRepository } from "./UsersNamespacesRepository";
import { UserParticipationInNamespace } from "./sharedTypes/UserInNamespaceModel";


type UserParticipation = UserManagement.UserParticipation

interface ParticipationsDiff {
    added: UserParticipation[],
    removed: UserParticipation[],
    modified: UserParticipation[],
}

@injectable()
export class NamespacesAggregatesRepository {
    namespacesRepository: NamespacesRepository
    usersNamespacesRepository: UsersNamespacesRepository

    public constructor(
        namespacesRepository: NamespacesRepository,
        usersNamespacesRepository: UsersNamespacesRepository) {
        this.namespacesRepository = namespacesRepository
        this.usersNamespacesRepository = usersNamespacesRepository
    }


    public async getById(ref: NamespaceRef): Promise<UserManagement.NamespaceAggregate | null> {
        const namespaceRow = await this.namespacesRepository.getNamespaceById(ref.id);
        if (namespaceRow == null) {
            return null
        }
        return this.buildAggregate(namespaceRow);
    }

    public async save(aggregate: UserManagement.NamespaceAggregate): Promise<void> {
        const props = aggregate.props();


        const rolesDiff = this.diff(props.initial.usersAndRoles, props.current.usersAndRoles);

        const pairToEntity = (pair: UserParticipation): UserParticipationInNamespace => ({
            namespace: { id: aggregate.namespace.id },
            role: pair.role,
            user: { id: pair.id },
            displayName: pair.displayName
        })
        const query = await this.usersNamespacesRepository.generateActions({
            added: rolesDiff.added.map(pairToEntity),
            modified: rolesDiff.modified.map(pairToEntity),
            removed: rolesDiff.removed.map(pairToEntity),
        })
        return this.usersNamespacesRepository.applyActions(query)
    }

    public diff(
        startState: Map<UserRef['id'], UserParticipation>,
        endState: Map<UserRef['id'], UserParticipation>): ParticipationsDiff {

        const result: ParticipationsDiff = {
            added: [],
            removed: [],
            modified: [],
        }

        endState.forEach((v, k) => {
            if (!startState.has(k)) {
                result.added.push(v)
            } else if (startState.get(k) != v) {
                result.modified.push(v)
            }
        })
        startState.forEach((v, k) => {
            if (!endState.has(k)) {
                result.removed.push(v)
            }
        })


        return result
    }

    async buildAggregate(namespace: Namespace): Promise<UserManagement.NamespaceAggregate> {
        const users = await this.usersNamespacesRepository.getNamespaceUsers({ id: namespace.id });

        return new UserManagement.NamespaceAggregate({
            usersAndRoles: new Map(users.map(e => ([e.user.id, { id: e.user.id, role: e.role, displayName: e.displayName }]))),
            namespace: namespace
        });
    }

}