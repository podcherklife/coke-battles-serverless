import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { inject, injectable } from "tsyringe";
import { Namespace, NamespaceRef, NamespaceSchema } from "./sharedTypes/Namespace";
import { AbstractRepository, UniquenessItem } from "./AbstractRepository";
import { DynamoDbProperties, DynamoDbPropertiesInjectionToken } from "./DynamoDbProperties";
import { Lsi1, PK } from "./indices";

type NamespaceIdType = Namespace['id'];
const namespaceEntityName = "namespace";

const defaultPk = "namespace";
const skPrefix = "namespace#";
const lsi1Prefix = "namespace_by_name#";
const uniqueNameEntity = "namespace_name_unique";
const uniqueNamePkPrefix = `${uniqueNameEntity}#`;
const uniqueNameSk = `${uniqueNameEntity}_sk`;

export function generatePkProperties(namespaceOrId: NamespaceRef | NamespaceIdType): PK {
    return {
        pk: `${defaultPk}`,
        sk: `${skPrefix}${typeof namespaceOrId == "string" ? namespaceOrId : namespaceOrId.id}`
    };
}

@injectable()
export class NamespacesRepository extends AbstractRepository<Namespace> {
    dynamoDb: DynamoDBDocument

    constructor(
        dynamoDb: DynamoDBDocument,
        @inject(DynamoDbPropertiesInjectionToken) dynamoDbProperties: DynamoDbProperties) {
        super(namespaceEntityName, dynamoDb, dynamoDbProperties.mainTableName);
        this.dynamoDb = dynamoDb;
    }

    async saveNamespace(namespace: Namespace): Promise<void> {
        return super.save(namespace);
    }

    async getNamespaceById(namespaceId: NamespaceIdType): Promise<Namespace | null> {
        return super.get(this.generatePkProperties(namespaceId));
    }

    protected generatePkProperties = generatePkProperties;

    protected generateUniquenessItems(item: Namespace): UniquenessItem[] {
        return [{
            tableName: this.tableName,
            key: {
                pk: `${uniqueNamePkPrefix}${item.name}`,
                sk: uniqueNameSk
            },
            entityType: uniqueNameEntity
        }];
    }

    protected generateLsi1Properties(namespace: Namespace): Lsi1 {
        return this.lsi1(`${lsi1Prefix}${namespace.name}`);
    }

    protected stripProperties(item: Namespace) {
        return NamespaceSchema.parse(item);
    }
}
