
export interface DynamoDbProperties {
    mainTableName: string
}

export const DynamoDbPropertiesInjectionToken = "DynamoDbProperties";