import { DynamoDBDocument, QueryCommandInput } from "@aws-sdk/lib-dynamodb";
import { inject, injectable } from "tsyringe";
import * as zod from "zod";
import { BaseUser, BaseUserSchema, UserRef } from "./sharedTypes/User";
import { DynamoDbProperties, DynamoDbPropertiesInjectionToken } from "./DynamoDbProperties";
import { EntityType, Lsi1, Lsi1Name, Lsi2, Lsi2Name, PK } from "./indices";

export type UserKey = QueryCommandInput['ExclusiveStartKey'];


type SavedUser = BaseUser & PK & Lsi1 & Lsi2 & EntityType;

export function generatePkProperties(user: UserRef): PK {
    return {
        pk: userPk,
        sk: `${userSkPrefix}${user.id}`
    }
}


@injectable()
export class UsersRepository {
    dynamoDb: DynamoDBDocument
    usersTableName: string;

    constructor(dynamoDb: DynamoDBDocument, @inject(DynamoDbPropertiesInjectionToken) dynamoDbProperties: DynamoDbProperties) {
        this.dynamoDb = dynamoDb;
        this.usersTableName = dynamoDbProperties.mainTableName;
    }

    async scanForUsers(
        usernameSearchString: string,
        startKey: UserKey): Promise<{ Items: BaseUser[], LastEvaluatedKey?: UserKey }> {

        return this.dynamoDb.query({
            IndexName: Lsi1Name,
            ExpressionAttributeValues: {
                ":searchString": `user_by_name#${usernameSearchString.toLowerCase()}`,
                ":user_pk": userPk
            },
            ExpressionAttributeNames: {
                "#user_by_name": "lsi1"
            },
            ExclusiveStartKey: startKey,
            TableName: this.usersTableName,
            Limit: UsersRepository.SCAN_SIZE,
            KeyConditionExpression: "pk = :user_pk and begins_with(#user_by_name, :searchString)"
        }).then(result => ({
            LastEvaluatedKey: result.LastEvaluatedKey,
            Items: zod.array(BaseUserSchema).parse(result.Items || [])
        }));
    };

    async getUserById(userId: string): Promise<BaseUser | null> {
        return this.dynamoDb.query({
            TableName: this.usersTableName,
            KeyConditionExpression: `pk = :user_pk and sk = :userId`,
            ExpressionAttributeValues: {
                ":userId": `${userSkPrefix}${userId}`,
                ":user_pk": userPk
            }
        }).then(result => result.Items && result.Items.length > 0 ? BaseUserSchema.parse(result.Items[0]) : null)
    }

    async getByIds(userIds: Set<string>): Promise<Map<BaseUser['id'], BaseUser>> {
        if (userIds.size == 0) {
            return new Map();
        }
        const getResult = await this.dynamoDb.batchGet({
            RequestItems: {
                [this.usersTableName]: {
                    Keys: Array.from(userIds.keys()).map(id => ({
                        "pk": userPk,
                        "sk": `${userSkPrefix}${id}`
                    }))
                }
            }
        });
        return zod.array(BaseUserSchema).parse(getResult.Responses![this.usersTableName] || []).reduce((a, e) => {
            a.set(e.id, e);
            return a;
        }, new Map());
    }

    async getUserByEmail(email: string): Promise<BaseUser | null> {
        return this.dynamoDb.query({
            TableName: this.usersTableName,
            IndexName: Lsi2Name,
            KeyConditionExpression: `pk = :user_pk and #user_by_email_key = :user_by_email_value`,
            ExpressionAttributeValues: {
                ":user_by_email_value": `user_by_email#${email.toLowerCase()}`,
                ":user_pk": userPk
            },
            ExpressionAttributeNames: {
                "#user_by_email_key": "lsi2"
            }
        }).then(result => result.Items && result.Items.length > 0 ? BaseUserSchema.parse(result.Items[0]) : null);
    }

    async saveUser(user: BaseUser): Promise<void> {
        return this.dynamoDb.put({
            TableName: this.usersTableName,
            Item: this.generateSavedUser(BaseUserSchema.parse(user))
        }).then(() => void 0);
    }

    private generateSavedUser(user: BaseUser): SavedUser {
        return {
            ...user,
            ...this.generateLsi1Properties(user),
            ...this.generateLsi2Properties(user),
            ...generatePkProperties(user),
            entityType: userEntityType
        }
    }

    private generateLsi1Properties(user: BaseUser): Lsi1 {
        return {
            lsi1: `user_by_name#${user.username.toLowerCase()}`
        };
    }

    private generateLsi2Properties(user: BaseUser): Lsi2 {
        return {
            lsi2: `user_by_email#${(user?.email || "EMPTY").toLowerCase()}`
        };
    }

    static SCAN_SIZE = 5
}


const userSkPrefix = "user#";
const userPk = "user";
const userEntityType = "user";