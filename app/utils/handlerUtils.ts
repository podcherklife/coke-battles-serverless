import { Handler } from "aws-lambda";
import { Lambda } from "@aws-sdk/client-lambda";

type QueryOnlyLambdaHttpEvent<TQuery extends ProcessedQuery> = {
    principalId?: string,
    query: RawQuery<TQuery>,
}

type ProcessedQuery = {
    [key: string]: string | number | boolean;
}
type RawQuery<T> = {
    [k in keyof T]: string;
}
type QueryValidator<T extends ProcessedQuery> = {
    [k in keyof T]: (val: string) => T[k]
}
export function validatingQueryHandler<
    TQuery extends ProcessedQuery,
    TResult>
    (func: (
        event: {
            principalId?: string,
            query: TQuery
        }) => (void | Promise<TResult>),
        validator: QueryValidator<TQuery>): Handler<QueryOnlyLambdaHttpEvent<RawQuery<TQuery>>> {

    return async (evt) => {
        const validatedQuery: TQuery = {} as TQuery;
        for (let key in validator) {
            const specificParamValidator = validator[key];
            const valueToValidate = evt.query[key];
            const validatedValue = specificParamValidator(valueToValidate);
            validatedQuery[key] = validatedValue;
        }

        return func({ ...evt, query: validatedQuery });
    };
}

export class KnownError extends Error {
    public readonly message: string
    constructor(message: string) {
        super();
        this.message = message;
    }
}
export class BadRequest extends KnownError {
    constructor(message: string) {
        super(message);
    }
}

export class NotFound extends KnownError {
    constructor(message: string) {
        super(message);
    }
}

export class AccessError extends KnownError {
    constructor(message: string) {
        super(message);
    }
}

export function errorToHttpStatusConvertingHandler<T>(simpleHandler: Handler<T>): Handler<T> {
    function handleError(error: any) {
        if (error instanceof KnownError) {
            console.log(error);
            if (error instanceof BadRequest) {
                throw new Error(`[400]: ${error.message}`);
            } else if (error instanceof NotFound) {
                throw new Error(`[404]: ${error.message}`);
            } else if (error instanceof AccessError) {
                throw new Error(`[403]: ${error.message}`);
            } else {
                throw new Error(`[500]`);
            }
        } else {
            if (error instanceof Error) {
                console.error(error);
            } else {
                console.error(`Failed to handle request: '${JSON.stringify(error)}'`);
            }
            throw new Error("[500]");
        }
    }
    return (event, ctx, callback) => {
        try {
            const result = simpleHandler(event, ctx, callback);
            if (result instanceof Promise) {
                return result.catch(handleError);
            }
        } catch (error) {
            handleError(error);
        }
    }
}


export const queryMappers = {
    toBool: (str?: string) => {
        if (str === "true") {
            return true;
        } else if (str === "false") {
            return false
        } else {
            throw new BadRequest(`Failed to convert value '${str}' to boolean`);
        }
    },
    toInt: (str?: string) => {
        if (str != null) {
            const parsed = Number.parseInt(str)
            if (!isNaN(parsed)) {
                return parsed;
            }
        }
        throw new BadRequest(`Failed to convert value '${str}" to int`);
    },
    pass: (str?: string) => str || "",
    require: (str?: string) => {
        if (str == null || str.length == 0) {
            throw new BadRequest("Parameter is null");
        } else {
            return str;
        }
    },
}

export function createLambdaInvoker<TEvent, TResult>(region: string, lambdaName: string, _: (event: TEvent) => Promise<TResult>) {
    return (event: TEvent) => {
        console.log(`Invoking lambda '${lambdaName}'`)
        var lambda = new Lambda({
            region: region
        });
        return new Promise<TResult>((resolve, reject) =>
            lambda.invoke({
                FunctionName: lambdaName,
                Payload: JSON.stringify(event)
            }, function (error, data) {
                console.log(`Finished invocation of '${lambdaName}`);
                if (error != null) {
                    const message = `Failed to invoke lambda '${lambdaName}': ${error}`
                    console.error(message);
                    reject(error.message);
                } else if (data?.FunctionError != null) {
                    const message = `Failed to invoke lambda '${lambdaName}': ${data.FunctionError}`
                    console.error(message);
                    reject(message);
                } else {
                    if (data!.Payload == null) {
                        reject("Empty payload")
                    } else {
                        resolve(JSON.parse(Buffer.from(data!.Payload!).toString()));
                    }
                }
            })
        )
    }
}
