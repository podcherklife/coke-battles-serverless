
import { APIGatewayTokenAuthorizerHandler } from 'aws-lambda';
import jwt from 'jsonwebtoken';
import "reflect-metadata";
import rp from 'request-promise';
import { CreateOrUpdateCokeBattlesUser } from './operations/CreateOrUpdateCokeBattlesUser';
import { createLambdaInvoker } from './utils/handlerUtils';

export const deps = {
    saveUserProfileRegion: process.env.AWS_REGION,
    auth0Domain: process.env.AUTH0_DOMAIN,
    saveUserProfileLambdaName: process.env.SAVE_USER_PROFILE_LAMBDA,
    jwtVerifier: verifyJwtToken,
}
if (typeof deps.saveUserProfileRegion === "undefined") {
    throw new Error("Missing env variable AWS_REGION");
}
if (typeof deps.saveUserProfileLambdaName === "undefined") {
    throw new Error("Missing env variable SAVE_USER_PROFILE_LAMBDA");
}
if (typeof deps.auth0Domain === "undefined") {
    throw new Error("Missing env variable AUTH0_DOMAIN");
}
function generatePolicy(principalId: string, effect: string, resource: string) {
    return {
        principalId: principalId,
        policyDocument: {
            Version: '2012-10-17',
            Statement: [{
                Action: 'execute-api:Invoke',
                Effect: effect,
                Resource: resource,
            }]
        }
    }
}


async function verifyJwtToken(jwtToken: string, pubKey: string) {
    return new Promise((resolve, reject) => {
        jwt.verify(jwtToken, pubKey, { algorithms: ['RS256'] }, (err, decoded) => {
            if (err) {
                reject(err);
            } else {
                resolve(decoded);
            }
        });
    });
};

const saveUserProfileInvoker = createLambdaInvoker(
    deps.saveUserProfileRegion,
    deps.saveUserProfileLambdaName,
    CreateOrUpdateCokeBattlesUser.prototype.createOrUpdateCokeBattlesUser);

async function saveUserProfile(decodedTokenPayload: any): Promise<string> {
    return saveUserProfileInvoker({
        body: {
            email: decodedTokenPayload.email,
            username: decodedTokenPayload.nickname,
        }
    }).then(e => e.id);
}

export const handler: APIGatewayTokenAuthorizerHandler = async (event) => {
    if (!event.authorizationToken) {
        throw new Error("Token required");
    }

    const jwtToken = event.authorizationToken.split(' ')[1];
    if (!jwtToken) {
        throw new Error("Wrong token header format")
    }
    const decodedToken = jwt.decode(jwtToken, { complete: true, json: true });
    if (decodedToken == null) {
        throw new Error("Failed to decode jwt token");
    }

    return rp(`https://${deps.auth0Domain}/.well-known/jwks.json`)
        .then((jwks) => {
            const jwksKey = JSON.parse(jwks).keys[0];

            //Validate the algorithm
            if (!jwksKey) {
                throw new Error('No supported jwt keys');
            }
            //Validate the algorithm
            if (jwksKey.alg !== 'RS256' || decodedToken.header.alg !== 'RS256') {
                throw new Error('Invalid algorithm used, only RS256 supported');
            }

            //Validate the signing key
            if (!jwksKey.kid || decodedToken.header.kid !== jwksKey.kid) {
                throw new Error('Invalid signing algorithm');
            }

            //Validate the certificate
            if (!jwksKey.x5c[0]) {
                throw new Error('No certificate found');
            }

            const cert = `-----BEGIN CERTIFICATE-----\n${jwksKey.x5c[0]}\n-----END CERTIFICATE-----\n`;
            return cert;
        })
        .then((pubKey) => verifyJwtToken(jwtToken, pubKey))
        .then(() => {
            return saveUserProfile(decodedToken.payload).then((userId) => {
                console.log("kinda success")
                return generatePolicy(userId, 'allow', '*');
            }).catch(error => {
                console.log("Error while generating policy: " + JSON.stringify(error))
                throw error;
            })
        })
        .catch((err) => {
            console.log('Failed jwt verification: ', err, 'auth: ', event.authorizationToken);
            throw err;
        });
};