import { inject, injectable } from 'tsyringe';
import { CokeBattlesRepository } from '../persistence/CokeBattlesRepository';
import { NamespacesRepository } from '../persistence/NamespacesRepository';
import { UsersRepository } from '../persistence/UsersRepository';
import { CokeBattle, ParticipationSideMinimal } from "../persistence/sharedTypes/CokeBattle";
import { UserRef } from '../persistence/sharedTypes/User';
import { AccessError, BadRequest } from "../utils/handlerUtils";
import { CreateOrUpdateCokeBattleTypes as Types } from './types/CreateOrUpdateCokeBattleTypes';
import { ParticipationSide } from './types/shared';

@injectable()
export class CreateOrUpdateCokeBattle {
    cokeBattlesRepository: CokeBattlesRepository;
    usersRepository: UsersRepository;
    namespacesRepository: NamespacesRepository;
    idGenerator: () => string;

    constructor(
        cokeBattlesRepository: CokeBattlesRepository,
        usersRepository: UsersRepository,
        namespacesRepository: NamespacesRepository,
        @inject("IdGenerator") idGenerator: () => string) {

        this.cokeBattlesRepository = cokeBattlesRepository;
        this.usersRepository = usersRepository;
        this.namespacesRepository = namespacesRepository;
        this.idGenerator = idGenerator;
    }

    async generateValidParticipant(participant?: UserRef): Promise<UserRef> {
        if (participant == null || participant.id == null) {
            throw new BadRequest(`Invalid participant: ${JSON.stringify(participant)}`)
        }
        let storedUser = await this.usersRepository.getUserById(participant.id);
        if (storedUser == null) {
            throw new BadRequest(`Invalid user: ${JSON.stringify(participant)}`);
        } else {
            return {
                id: storedUser.id
            };
        }
    }

    async generateValidSide(side?: ParticipationSide): Promise<ParticipationSideMinimal> {
        if (side == null) {
            throw new BadRequest(`Invalid side: ${JSON.stringify(side)}`);
        } else {
            const participants = await Promise.all((side.participants || []).map(e => this.generateValidParticipant({ id: e.userId })));
            return {
                description: side.description,
                participants: participants
            }
        }
    }

    async generateValidSides(inputSides?: ParticipationSide[]): Promise<ParticipationSideMinimal[]> {
        const sides = await Promise.all((inputSides || []).map(e => this.generateValidSide(e)));
        return sides;
    }



    async createOrUpdateCokeBattle(event: { principalId: string } & Types.Input): Promise<Types.Output> {
        let newBattle = Types.CokeBattleInputSchema.parse(event.body);

        const currentUserId: string = event.principalId;

        const namespacePromise = this.namespacesRepository.getNamespaceById(newBattle.namespaceId);
        const currentUserPromise = this.usersRepository.getUserById(currentUserId);
        const namespace = await namespacePromise;
        const currentUser = await currentUserPromise;
        if (namespace == null) {
            throw new BadRequest("Could not find namespace with id");
        }
        if (currentUser == null) {
            throw new AccessError("Failed to find current user")
        }

        let battleId: CokeBattle['id'];
        let creationDate: number | undefined = undefined;
        if (newBattle.battleId != null) {
            battleId = newBattle.battleId;
            let oldBattle = await this.cokeBattlesRepository.getBattleById(namespace.id, battleId);
            if (oldBattle == null) {
                throw new BadRequest(`Could not find battle with id '${battleId}'`);
            } else if (oldBattle.creator.id != currentUserId) {
                throw new AccessError("Only creator can modify ");
            } else {
                creationDate = oldBattle.creationDate;
            }
        } else {
            battleId = this.idGenerator();
        }

        const validatedSidesPromise = this.generateValidSides(newBattle.sides);

        const validatedSides = await validatedSidesPromise;

        await this.cokeBattlesRepository.saveBattle({
            id: battleId,
            description: newBattle.description,
            creator: {
                id: currentUser.id,
            },
            creationDate: creationDate ?? Math.floor(new Date().getTime() / 1000),
            expectedResolutionDate: newBattle.expectedResolutionDate,
            sides: validatedSides,
            namespace: {
                id: namespace.id
            }
        });
    };
}

