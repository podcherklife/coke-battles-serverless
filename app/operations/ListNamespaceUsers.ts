import { injectable } from "tsyringe";
import { NamespacesRepository } from "../persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../persistence/UsersNamespacesRepository";
import { UsersRepository } from "../persistence/UsersRepository";
import { NotFound } from "../utils/handlerUtils";
import { ListNamespaceUsersTypes as Types } from "./types/ListNamespaceUsersTypes";

@injectable()
export class ListNamespaceUsers {
    usersRepository: UsersRepository;
    namespacesRepository: NamespacesRepository;
    usersNamespacesRepository: UsersNamespacesRepository;

    constructor(usersRepository: UsersRepository, namespacesRepository: NamespacesRepository, usersNamespacesRepository: UsersNamespacesRepository) {
        this.usersRepository = usersRepository;
        this.namespacesRepository = namespacesRepository;
        this.usersNamespacesRepository = usersNamespacesRepository;
    }

    async listNamespaceUsers(event: Types.Input): Promise<Types.Output> {
        //FIXME: add permissions
        let namespaceId = event.query.namespaceId;
        let searchString = event.query.searchString;
        let namespace = await this.namespacesRepository.getNamespaceById(namespaceId);
        if (namespace == null) {
            throw new NotFound(`Namespace ${namespaceId} not found`);
        }
        if (searchString == null || searchString == "") {
            return this.getAllUsers(namespaceId);
        } else {
            return this.searchUsersByString(namespaceId, searchString);
        }
    };

    async searchUsersByString(namespaceId: string, searchString: string): Promise<Types.Output[number][]> {
        return this.usersNamespacesRepository.searchNamespaceUsersByName({ id: namespaceId }, searchString)
            .then(e => e.map(u => ({ userId: u.user.id, role: u.role, username: u.displayName, namespaceId, })));
    }

    async getAllUsers(namespaceId: string): Promise<Types.Output[number][]> {
        let namespaceUsers = await this.usersNamespacesRepository.getNamespaceUsers({ id: namespaceId });
        return namespaceUsers.map(namespaceUser => ({
            userId: namespaceUser.user.id,
            username: namespaceUser.displayName,
            role: namespaceUser.role,
            namespaceId,
        }));
    }

}
