import { inject, injectable } from 'tsyringe';
import { UsersRepository } from '../persistence/UsersRepository';
import { RegisteredUser } from "../persistence/sharedTypes/User";
import { CreateOrUpdateCokeBattlesUsersTypes as Types } from './types/CreateOrUpdateCokeBattlesUser';

@injectable()
export class CreateOrUpdateCokeBattlesUser {
    usersRepository: UsersRepository;
    idGenerator: () => string;

    constructor(usersRepository: UsersRepository, @inject("IdGenerator") idGenerator: CreateOrUpdateCokeBattlesUser['idGenerator']) {
        this.usersRepository = usersRepository
        this.idGenerator = idGenerator;
    }

    async createOrUpdateCokeBattlesUser(event: Types.Input): Promise<Types.Output> {
        //FIXME: add user role check
        const newUser = Types.CreateOrUpdateUserInputSchema.parse(event.body);

        let existingUser = await this.usersRepository.getUserByEmail(newUser.email);

        const profileToSave: RegisteredUser = {
            ...newUser,
            id: existingUser?.id || this.idGenerator(),
            type: "human"
        }
        await this.usersRepository.saveUser(profileToSave);
        return profileToSave;
    };
}
