import * as zod from "zod";
import { UserIdSchema } from "./shared";

export namespace CreateOrUpdateCokeBattlesUsersTypes {
    export const CreateOrUpdateUserInputSchema = zod.object({
        userId: UserIdSchema.optional(),
        username: zod.string(),
        email: zod.string(),
    })

    export type CreateOrUpdateUserInput = zod.infer<typeof CreateOrUpdateUserInputSchema>;

    export type Input = { body: CreateOrUpdateUserInput }
    export type Output = {
        id: string;
        username: string;
        email: string;
    }

}