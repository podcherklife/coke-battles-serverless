import { Namespace, NamespaceId } from "./shared"

export namespace GetNamespaceTypes {
    export type Input = {
        query: {
            namespaceId: NamespaceId
        }
    }
    export type Output = Namespace
}