import * as zod from "zod";
import { NamespaceId, NamespaceIdSchema } from "./shared";

export namespace CreateOrUpdateNamespaceTypes {
    export const NamespaceInputSchema = zod.object({
        name: zod.string(),
        namespaceId: NamespaceIdSchema.optional(),
    });

    export type NamespaceInput = zod.infer<typeof NamespaceInputSchema>;
    export type Input = {
        body: NamespaceInput
    }
    export type Output = NamespaceId
}