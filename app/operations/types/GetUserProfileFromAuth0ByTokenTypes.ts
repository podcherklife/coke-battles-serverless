import { BaseUser } from "../../persistence/sharedTypes/User";
import { UserId } from "./shared";

export namespace GetUserProfileFromAuth0ByTokenTypes {
    export type Input = {
        query: {
            accessToken: string
        }
    }
    export type Output = {
        id: UserId
        username: BaseUser['username']
        picture: string
    }
}