import * as zod from "zod";
import { NamespaceIdSchema, NamespaceParticipantRoleSchema, NamespaceSchema } from "./shared";


export namespace ListUserNamespacesTypes {
    const UsersNamespaceSchema = zod.object({
        namespaceId: NamespaceIdSchema,
        role: NamespaceParticipantRoleSchema,
        name: NamespaceSchema.shape.name,
    });
    type UsersNamespace = zod.infer<typeof UsersNamespaceSchema>;
    export type Input = {
        query: {
            userId: string
        }
    }
    export type Output = UsersNamespace[]
}