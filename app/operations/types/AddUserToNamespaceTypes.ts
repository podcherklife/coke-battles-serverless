import { NamespaceId, UserId } from "./shared";

export namespace AddUserToNamespaceTypes {
    export type Input = {
        query: {
            namespaceId: NamespaceId
            userId: UserId
        }
    }
    export type Output = void
}