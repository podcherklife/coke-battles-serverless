import * as zod from "zod";
import { NamespaceIdSchema, NamespaceParticipantRoleSchema, UserIdSchema, UserSchema } from "./shared";

export namespace ListNamespaceUsersTypes {
    const NamespaceParticipationSchema = zod.object({
        userId: UserIdSchema,
        namespaceId: NamespaceIdSchema,
        role: NamespaceParticipantRoleSchema,
        username: UserSchema.shape.username,
    })
    type NamespaceParticipation = zod.infer<typeof NamespaceParticipationSchema>;

    export type Input = {
        query: {
            namespaceId: string, searchString: string | null
        }
    }
    export type Output = NamespaceParticipation[]

}