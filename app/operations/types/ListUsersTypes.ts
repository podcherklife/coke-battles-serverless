import { User } from "./shared"

export namespace ListUsersTypes {
    export type Input = {
        query: {
            searchString: string
        }
    }
    export type Output = {
        username: User['username']
        userId: User['userId']
    }[]
}