import * as zod from "zod";

// user
export const UserIdSchema = zod.string();
export type UserId = zod.infer<typeof UserIdSchema>;

export const UserSchema = zod.object({
    userId: UserIdSchema,
    username: zod.string(),
    email: zod.string().nullable(),
});
export type User = zod.infer<typeof UserSchema>;

export const UserRefSchema = UserSchema.pick({
    userId: true
})
export type UserRef = zod.infer<typeof UserRefSchema>;



// battle
export const CokeBattleIdSchema = zod.string();
export type CokeBattleId = zod.infer<typeof CokeBattleIdSchema>;

export const CokeBattleSchema = zod.object({
    cokeBattleId: CokeBattleIdSchema,
    creationDate: zod.number(),
    expectedResolutionDate: zod.number().nullable(),
    creator: UserRefSchema,
    description: zod.string(),
});
export type CokeBattle = zod.infer<typeof CokeBattleSchema>;

export const ParticipationSideSchema = zod.object({
    description: zod.string().nullable(),
    participants: zod.array(UserRefSchema)
});
export type ParticipationSide = zod.infer<typeof ParticipationSideSchema>;


//namespace
export const NamespaceIdSchema = zod.string();
export type NamespaceId = zod.infer<typeof NamespaceIdSchema>;
export const NamespaceSchema = zod.object({
    namespaceId: NamespaceIdSchema,
    name: zod.string(),
    creator: UserRefSchema,
});
export type Namespace = zod.infer<typeof NamespaceSchema>;

export const NamespaceRefSchema = NamespaceSchema.pick({
    namespaceId: true,
});
export type NamespaceRef = zod.infer<typeof NamespaceRefSchema>;

// namespace participation
export const NamespaceParticipantRoleSchema = zod.literal("admin").or(zod.literal("participant"));
export type NamespaceParticipantRole = zod.infer<typeof NamespaceParticipantRoleSchema>;

export const NamespaceParticipant = zod.object({
    namespace: NamespaceRefSchema,
    user: UserRefSchema,
    role: NamespaceParticipantRoleSchema,
    displayName: UserSchema.shape.username
});
export type NamespaceParticipant = zod.infer<typeof NamespaceParticipant>;
