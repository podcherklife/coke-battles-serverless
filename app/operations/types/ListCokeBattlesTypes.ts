import * as zod from "zod";
import { CokeBattleIdSchema, NamespaceId } from "./shared";

export namespace ListCokeBattlesTypes {
    export const BattleParticipantSchema = zod.object({
        userId: zod.string(),
        username: zod.string(),
    })
    export type BattleParticipant = zod.infer<typeof BattleParticipantSchema>;

    export const ParticipationSideSchema = zod.object({
        description: zod.string().nullable(),
        participants: zod.array(BattleParticipantSchema)
    })

    export const CokeBattleSchema = zod.object({
        battleId: CokeBattleIdSchema,
        namespaceId: zod.string(),
        creator: BattleParticipantSchema,
        sides: zod.array(ParticipationSideSchema),
        creationDate: zod.number(),
        description: zod.string(),
        expectedResolutionDate: zod.number().nullable(),
    });
    export type CokeBattle = zod.infer<typeof CokeBattleSchema>;

    export const BattlesSchema = zod.array(CokeBattleSchema)

    export type Input = {
        query: {
            namespaceId: NamespaceId
        }
    }

    export type Output = zod.infer<typeof BattlesSchema>
}