import * as zod from "zod";
import { CokeBattleIdSchema, NamespaceIdSchema, ParticipationSideSchema } from "./shared";

export namespace CreateOrUpdateCokeBattleTypes {

    export const CokeBattleInputSchema = zod.object({
        battleId: CokeBattleIdSchema.optional(),
        namespaceId: NamespaceIdSchema,
        expectedResolutionDate: zod.number().nullable(),
        description: zod.string(),
        sides: zod.array(ParticipationSideSchema),
    })
    export type CokeBattleInput = zod.infer<typeof CokeBattleInputSchema>;
    export type Input = {
        body: CokeBattleInput
    }

    export type Output = void
}