import * as zod from "zod";
import { CokeBattleIdSchema, NamespaceIdSchema } from "./shared";

export namespace DeleteCokeBattleTypes {
    export const InputSchema = zod.object({
        body: zod.object({
            namespaceId: NamespaceIdSchema,
            battleId: CokeBattleIdSchema,
        })
    })
    export type Input = zod.infer<typeof InputSchema>;

    export type Output = void
}