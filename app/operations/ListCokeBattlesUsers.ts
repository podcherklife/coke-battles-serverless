import { injectable } from "tsyringe";
import { UserKey, UsersRepository } from "../persistence/UsersRepository";
import { BaseUser } from "../persistence/sharedTypes/User";
import { ListUsersTypes as Types } from "./types/ListUsersTypes";


@injectable()
export class ListCokeBattlesUsers {
    static DEFAULT_BATCH_SIZE = 5;

    usersRepository: UsersRepository;

    constructor(usersRepository: UsersRepository) {
        this.usersRepository = usersRepository;
    }

    async listCokeBattlesUsers(event: Types.Input): Promise<Types.Output> {
        //FIXME: add permissions checks
        const searchString = event.query.searchString;
        const expectedResultSize = ListCokeBattlesUsers.DEFAULT_BATCH_SIZE;

        let result: BaseUser[] = [];
        let lastEvaluatedKey = undefined;
        do {
            const scanResult: { Items: BaseUser[], LastEvaluatedKey?: UserKey } = await this.usersRepository.scanForUsers(searchString, lastEvaluatedKey);
            result = result.concat(scanResult.Items as BaseUser[])
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
        } while (result.length < expectedResultSize && lastEvaluatedKey != null)
        return (result.slice(0, expectedResultSize))
            .map(e => ({ username: e.username, userId: e.id }))
    };

}
