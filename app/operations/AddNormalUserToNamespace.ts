import { injectable } from 'tsyringe';
import { NamespacesAggregatesRepository } from '../persistence/NamespacesAggregatesRepository';
import { UsersRepository } from '../persistence/UsersRepository';
import { AccessError, BadRequest } from '../utils/handlerUtils';
import { AddUserToNamespaceTypes as Types } from './types/AddUserToNamespaceTypes';

@injectable()
export class AddNormalUserToNamespace {
    namespaceAggregatesRepository: NamespacesAggregatesRepository;
    usersRepository: UsersRepository;


    constructor(namespaceAggregatesRepository: NamespacesAggregatesRepository, usersRepository: UsersRepository) {
        this.namespaceAggregatesRepository = namespaceAggregatesRepository;
        this.usersRepository = usersRepository;
    }

    async handle(event: { principalId: string } & Types.Input): Promise<Types.Output> {
        const namespaceId = event.query.namespaceId;
        const targetUserId = event.query.userId;
        const currentUserId = event.principalId;

        const [users, namespace] = await Promise.all([
            this.usersRepository.getByIds(new Set([currentUserId, targetUserId])),
            this.namespaceAggregatesRepository.getById({ id: namespaceId }),
        ]);
        const targetUser = users.get(targetUserId);
        const currentUser = users.get(currentUserId)!;

        if (targetUser == null) {
            throw new BadRequest(`User ${targetUserId} not found`);
        }
        if (namespace == null) {
            throw new BadRequest(`Namespace ${namespaceId} not found`);
        }

        const callerRole = namespace.getUserRole(currentUser)

        if (currentUser.id != namespace.creator.id && callerRole != "admin") {
            throw new AccessError("Not authorized");
        } else {
            namespace.addUserParticipation({ id: targetUser.id, displayName: targetUser.username, role: "participant" })
            return this.namespaceAggregatesRepository.save(namespace)
        }
    };
}