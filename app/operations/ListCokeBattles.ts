import { injectable } from 'tsyringe';
import { CokeBattlesRepository } from '../persistence/CokeBattlesRepository';
import { UsersRepository } from '../persistence/UsersRepository';
import { UserId } from '../persistence/sharedTypes/User';
import { ListCokeBattlesTypes as Types } from './types/ListCokeBattlesTypes';

@injectable()
export class ListCokeBattles {
    cokeBattlesRepository: CokeBattlesRepository;
    usersRepository: UsersRepository;
    constructor(
        cokeBattlesRepository: CokeBattlesRepository,
        usersRepository: UsersRepository,
    ) {
        this.cokeBattlesRepository = cokeBattlesRepository;
        this.usersRepository = usersRepository;
    }
    async listCokeBattles(event: Types.Input): Promise<Types.Output> {
        // FIXME: add permissions check
        const battles = await this.cokeBattlesRepository.listBattles(event.query.namespaceId);

        const userResolver = new UserResolver(this.usersRepository);


        const resultWithUnresolvedParticipants = battles.map(b => ({
            battleId: b.id,
            creationDate: b.creationDate,
            expectedResolutionDate: b.expectedResolutionDate,
            description: b.description,
            creator: userResolver.register(b.creator.id),
            namespaceId: b.namespace.id,
            sides: b.sides.map(s => ({
                description: s.description,
                participants: s.participants.map(p => (userResolver.register(p.id)))
            })),
        }))
        await userResolver.resolve();
        // now users are resolved
        const result = resultWithUnresolvedParticipants;
        return result;
    };
}



class UserResolver {
    usersRepository: UsersRepository;
    registeredRefs: Map<string, Types.BattleParticipant> = new Map();

    constructor(usersRepository: UsersRepository) {
        this.usersRepository = usersRepository;
    }

    register(userId: UserId): Types.BattleParticipant {
        let unresolvedUser = this.registeredRefs.get(userId);
        if (unresolvedUser == null) {
            unresolvedUser = {
                userId
            } as Types.BattleParticipant;
            this.registeredRefs.set(userId, unresolvedUser);
        }
        return unresolvedUser;
    }

    async resolve(): Promise<void> {
        const userIds = Array.from(this.registeredRefs.keys());
        if (userIds.length == 0) {
            return;
        }

        return this.usersRepository.getByIds(new Set(this.registeredRefs.keys()))
            .then(result => result.forEach(dbUser => {
                this.registeredRefs.get(dbUser.id)!.username = dbUser.username;
            }))
    }
}