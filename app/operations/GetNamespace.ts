import { injectable } from "tsyringe";
import { NamespacesRepository } from "../persistence/NamespacesRepository";
import { NotFound } from "../utils/handlerUtils";
import { GetNamespaceTypes as Types } from "./types/GetNamespaceTypes";

@injectable()
export class GetNamespace {
    namespacesRepository: NamespacesRepository;

    constructor(namespacesRepository: NamespacesRepository) {
        this.namespacesRepository = namespacesRepository;
    }

    async getNamespace(event: Types.Input): Promise<Types.Output> {
        const namespaceId = event.query.namespaceId;

        let namespace = await this.namespacesRepository.getNamespaceById(namespaceId);

        if (namespace == null) {
            throw new NotFound(`Namespace '${namespaceId}' not found`);
        }
        return { creator: { userId: namespace.creator.id }, namespaceId: namespace.id, name: namespace.name };
    };
}