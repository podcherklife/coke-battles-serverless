import { Handler } from "aws-lambda";
import { injectable } from "tsyringe";
import { CokeBattlesRepository } from "../persistence/CokeBattlesRepository";
import { AccessError, NotFound } from "../utils/handlerUtils";
import { DeleteCokeBattleTypes as Types } from "./types/DeleteCokeBattleTypes";

@injectable()
export class DeleteCokeBattle {
    cokeBattlesRepository: CokeBattlesRepository;

    constructor(cokeBattlesRepository: CokeBattlesRepository) {
        this.cokeBattlesRepository = cokeBattlesRepository;
    }

    deleteCokeBattle: Handler = async (event: { principalId: string } & Types.Input): Promise<Types.Output> => {
        let deleteRequest = event.body;
        let currentUser = event.principalId;

        let namespaceId = deleteRequest.namespaceId;
        let battleId = deleteRequest.battleId;

        let savedBattle = await this.cokeBattlesRepository.getBattleById(namespaceId, battleId);

        if (!savedBattle) {
            throw new NotFound(`Battle '${battleId} not found`);
        }
        if (savedBattle.creator.id != currentUser) {
            throw new AccessError("Only battle creator can delete the battle")
        }
        return this.cokeBattlesRepository.deleteBattle(namespaceId, battleId);
    };
}
