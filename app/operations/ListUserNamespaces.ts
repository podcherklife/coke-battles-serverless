import { injectable } from "tsyringe";
import { NamespacesRepository } from "../persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../persistence/UsersNamespacesRepository";
import { UsersRepository } from "../persistence/UsersRepository";
import { NotFound } from "../utils/handlerUtils";
import { ListUserNamespacesTypes as Types } from "./types/ListUserNamespacesTypes";

@injectable()
export class ListUserNamespaces {
    usersRepository: UsersRepository;
    namespacesRepository: NamespacesRepository;
    usersNamespacesRepository: UsersNamespacesRepository;

    constructor(usersRepository: UsersRepository, namespacesRepository: NamespacesRepository, usersNamespacesRepository: UsersNamespacesRepository) {
        this.usersRepository = usersRepository;
        this.namespacesRepository = namespacesRepository;
        this.usersNamespacesRepository = usersNamespacesRepository;
    }

    async listUserNamespaces(event: Types.Input): Promise<Types.Output> {
        let userId = event.query.userId;
        let user = await this.usersRepository.getUserById(userId);
        if (user == null) {
            throw new NotFound(`User ${userId} not found`);
        }
        let userNamespaces = await this.usersNamespacesRepository.getUserNamespaces({ id: user.id })
        return Promise.all(userNamespaces
            .map(userNamespace => this.namespacesRepository.getNamespaceById(userNamespace.namespace.id).then(ns => [userNamespace, ns!] as const)))
            .then(pairs => pairs.map(([uns, ns]) => ({ userId: uns.user.id, name: ns.name, role: uns.role, namespaceId: ns.id })));
    };

}
