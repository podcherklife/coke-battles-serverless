import { inject, injectable } from "tsyringe";
import { ConditionCheckError } from "../persistence/AbstractRepository";
import { NamespacesRepository } from "../persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../persistence/UsersNamespacesRepository";
import { UsersRepository } from "../persistence/UsersRepository";
import { AccessError, BadRequest } from "../utils/handlerUtils";
import { CreateOrUpdateNamespaceTypes as Types } from "./types/CreateOrUpdateNamespaceTypes";

@injectable()
export class CreateOrUpdateNamespace {
    namespacesRepository: NamespacesRepository;
    usersRepository: UsersRepository;
    usersNamespacesRepository: UsersNamespacesRepository;
    idGenerator: () => string;

    constructor(
        namespacesRepository: NamespacesRepository,
        usersRepository: UsersRepository,
        usersNamespacesRepository: UsersNamespacesRepository,
        @inject("IdGenerator") idGenerator: CreateOrUpdateNamespace['idGenerator']) {

        this.namespacesRepository = namespacesRepository;
        this.usersRepository = usersRepository;
        this.usersNamespacesRepository = usersNamespacesRepository;
        this.idGenerator = idGenerator;
    }

    async createOrUpdateNamespace(event: { principalId: string } & Types.Input): Promise<Types.Output> {
        const namespace = Types.NamespaceInputSchema.parse(event.body);
        const currentUserId = event.principalId;
        const currentUser = await this.usersRepository.getUserById(currentUserId);
        if (currentUser == null) {
            throw Error(`Failed to find user ${currentUserId}`);
        }

        if (namespace.namespaceId != null) {
            const savedNamespace = await this.namespacesRepository.getNamespaceById(namespace.namespaceId);
            if (savedNamespace == null) {
                throw new BadRequest(`Could not find namespace with id ${namespace.namespaceId}`);
            } else if (savedNamespace.creator.id != currentUserId) {
                throw new AccessError(`Only creator is allowed to modify namespace`);
            } else {
                await this.namespacesRepository.saveNamespace({
                    ...savedNamespace,
                    name: namespace.name,
                }).catch(this.rethrowConditionCheckErrorAsBadRequest);
                return savedNamespace.id;
            }
        } else {
            const generatedId = this.idGenerator();
            await this.namespacesRepository.saveNamespace({
                ...namespace,
                id: generatedId,
                creator: {
                    id: currentUserId
                }
            }).catch(this.rethrowConditionCheckErrorAsBadRequest);

            await this.usersNamespacesRepository.saveUserInNamespace({
                namespace: { id: generatedId },
                user: { id: currentUserId },
                role: "admin",
                displayName: currentUser.username
            });
            return generatedId;
        }
    };

    rethrowConditionCheckErrorAsBadRequest(e: any) {
        if (e instanceof ConditionCheckError) {
            throw new BadRequest(`Namespace with this name already exists`);
        } else {
            throw e;
        }
    }
}