import { injectable } from 'tsyringe';
import { NamespacesAggregatesRepository } from '../persistence/NamespacesAggregatesRepository';
import { UsersRepository } from '../persistence/UsersRepository';
import { AccessError, BadRequest } from '../utils/handlerUtils';
import { RemoveUserFromNamespaceTypes as Types } from './types/RemoveUserFromNamespaceTypes';

@injectable()
export class RemoveUserFromNamespace {
    usersRepository: UsersRepository;
    namespacesAggregateRepository: NamespacesAggregatesRepository;

    constructor(
        namespaceAggregatesRepository: NamespacesAggregatesRepository,
        usersRepository: UsersRepository
    ) {
        this.namespacesAggregateRepository = namespaceAggregatesRepository;
        this.usersRepository = usersRepository;
    }

    async removeUserFromNamespace(event: { principalId: string } & Types.Input): Promise<Types.Output> {
        const namespaceId = event.query.namespaceId;
        const targetUserId = event.query.userId;
        const currentUserId = event.principalId;

        const [users, namespace] = await Promise.all([
            this.usersRepository.getByIds(new Set([currentUserId, targetUserId])),
            this.namespacesAggregateRepository.getById({ id: namespaceId })
        ]);
        const targetUser = users.get(targetUserId);
        const currentUser = users.get(currentUserId)!;

        if (targetUser == null) {
            throw new BadRequest(`User ${targetUserId} not found`);
        }
        if (namespace == null) {
            throw new BadRequest(`Namespace ${namespaceId} not found`);
        }

        if (currentUser.id != namespace.creator.id && namespace.getUserRole(currentUser) != "admin") {
            throw new AccessError("Only namespace creator or admin can add or remove other users from namespaces");
        } else {
            namespace.removeUser({ id: targetUserId });
            return this.namespacesAggregateRepository.save(namespace);
        }
    }
}


