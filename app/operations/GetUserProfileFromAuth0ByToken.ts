import fetch from 'request-promise';
import { inject, injectable } from 'tsyringe';
import { UsersRepository } from '../persistence/UsersRepository';
import { GetUserProfileFromAuth0ByTokenTypes as Types } from './types/GetUserProfileFromAuth0ByTokenTypes';

type Fetch = typeof fetch;

@injectable()
export class GetUserProfileFromAuth0ByToken {
    usersRepository: UsersRepository;
    auth0Domain: string;
    fetch: Fetch;

    constructor(
        usersRepository: UsersRepository,
        @inject("Fetch") fetch: Fetch,
        @inject("Auth0Domain") auth0Domain: string) {
        this.usersRepository = usersRepository;
        this.auth0Domain = auth0Domain;
        this.fetch = fetch;
    }

    async getProfile(event: { principalId: string } & Types.Input): Promise<Types.Output> {
        const accessToken = event.query.accessToken;
        const auth0Domain = process.env.AUTH0_DOMAIN;

        if (!accessToken) {
            throw new Error('AccessToken not found');
        }

        const options = {
            url: `https://${auth0Domain}/userinfo`,
            method: 'POST',
            json: true,
            headers: {
                'Authorization': `Bearer ${accessToken}`
            }
        };

        return this.fetch(options).then((body) => {
            return Promise.all([Promise.resolve(body), this.usersRepository.getUserById(event.principalId)]);
        }).then(([zeroAuthProfile, cokeBattlesUser]) => {
            if (zeroAuthProfile == null) {
                throw new Error("Failed to fetch authZeroProfile");
            }
            if (cokeBattlesUser == null) {
                throw new Error(`No user profile for id '${event.principalId}'`);
            }
            return {
                id: cokeBattlesUser.id,
                username: cokeBattlesUser.username,
                picture: zeroAuthProfile.picture
            };
        })
    };
}