import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import fetch from "request-promise";
import { container } from "tsyringe";
import * as UUID from "uuid";
import { DynamoDbProperties } from "./persistence/DynamoDbProperties";

container.registerInstance(DynamoDBDocument as any, DynamoDBDocument.from(new DynamoDBClient({})));
container.registerInstance("Auth0Domain", process.env.AUTH0_DOMAIN);
container.registerInstance("IdGenerator", UUID.v4)
container.registerInstance("Fetch", fetch);
container.registerInstance("DynamoDbProperties", {
    mainTableName: process.env.DYNAMO_DB_MAIN_TABLE_NAME
} as DynamoDbProperties)
