import { Namespace } from "../persistence/sharedTypes/Namespace";
import { UserRef } from "../persistence/sharedTypes/User";
import { UserInNamespaceRole, UserParticipationInNamespace } from "../persistence/sharedTypes/UserInNamespaceModel";

export namespace UserManagement {

    export interface NamespaceAggregateProps {
        usersAndRoles: Map<UserRef['id'], UserParticipation>,
        namespace: Namespace
    }

    export interface UserParticipation {
        id: UserRef['id'],
        role: UserInNamespaceRole,
        displayName: UserParticipationInNamespace['displayName']
    }

    export class NamespaceAggregate {

        usersAndRoles: Map<UserRef['id'], UserParticipation>
        namespace: Namespace

        initialProps: NamespaceAggregateProps
        creator: UserRef

        constructor(props: NamespaceAggregateProps) {
            this.initialProps = props
            this.usersAndRoles = new Map(props.usersAndRoles);
            this.namespace = props.namespace
            this.creator = this.namespace.creator
        }

        public removeUser(user: UserRef): void {
            if (!this.usersAndRoles.has(user.id)) {
                throw new Error(`User ${user} is not a part of this namespace`);
            }
            this.usersAndRoles.delete(user.id);
        }

        public addUserParticipation(participation: UserParticipation): void {
            const currentParticipation = this.usersAndRoles.get(participation.id)
            if (currentParticipation != null) {
                throw new Error(`User is already participating in this namespace: ${currentParticipation.role}`)
            }
            this.usersAndRoles.set(participation.id, participation)
        }

        public modifyUserParticipation(participation: UserParticipation): void {
            const currentParticipation = this.getParticipation({ id: participation.id })
            if (currentParticipation != null
                && (currentParticipation.displayName != participation.displayName
                    || currentParticipation.role != participation.role)) {

                this.usersAndRoles.set(participation.id, { ...participation, role: participation.role || this.defaultRole() })
            } else {
                throw new Error(`User ${participation.id} has no current participation`)
            }
        }

        public hasUser(user: UserRef): boolean {
            return this.usersAndRoles.has(user.id)
        }

        public getUserRole(user: UserRef): UserInNamespaceRole | null {
            return this.getParticipation(user)?.role || null
        }

        getParticipation(user: UserRef): UserParticipation | null {
            return this.usersAndRoles.get(user.id) || null
        }

        public props(): { initial: NamespaceAggregateProps, current: NamespaceAggregateProps } {
            return {
                initial: this.initialProps,
                current: {
                    usersAndRoles: this.usersAndRoles,
                    namespace: this.namespace
                }
            }
        }

        private defaultRole(): UserInNamespaceRole {
            return "participant";
        }
    }
}