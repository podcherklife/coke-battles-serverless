terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws" 
    }
  }
  
  backend "http" {
  }

  required_version = ">= 1.0"
}

provider "aws" {
  skip_credentials_validation = true
  region  = "us-east-2"

  default_tags {
    tags = var.tags
  }
}

resource "aws_s3_bucket" "deployment_bucket" {
  bucket = var.deployment_bucket
}

resource "aws_dynamodb_table" "coke_battles_table" {
  name = var.coke_battles_dynamodb_table_name
  billing_mode = "PROVISIONED"
  hash_key = "pk"
  range_key = "sk"
  write_capacity = 2
  read_capacity = 3

  attribute {
    name = "pk"
    type = "S"
  }

  attribute {
    name = "sk"
    type = "S"
  }

  attribute {
    name = "pk1"
    type = "S"
  }

  attribute {
    name = "sk1"
    type = "S"
  }

  attribute {
    name = "lsi1"
    type = "S"
  }

  attribute {
    name = "lsi2"
    type = "S"
  }

  attribute {
    name = "lsi3"
    type = "S"
  }

  attribute {
    name = "lsi4"
    type = "S"
  }

  local_secondary_index {
    name = "pk-lsi1-index"
    range_key = "lsi1"
    projection_type = "ALL"
  }

  local_secondary_index {
    name = "pk-lsi2-index"
    range_key = "lsi2"
    projection_type = "ALL"
  }

  local_secondary_index {
    name = "pk-lsi3-index"
    range_key = "lsi3"
    projection_type = "ALL"
  }

  local_secondary_index {
    name = "pk-lsi4-index"
    range_key = "lsi4"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "pk1-sk1-index"
    hash_key = "pk1"
    range_key = "sk1"
    write_capacity = 2
    read_capacity = 3
    projection_type = "ALL"
  }

}
