variable "deployment_bucket" {
  type = string
  description = "Bucket to store serverless deployment data"
}

variable "tags" {
  default = {}
  description = "Additional resource tags"
  type = map(string)
}

variable "coke_battles_dynamodb_table_name" {
  type = string
  description = "Table for storing coke battles data"
}
