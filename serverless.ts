import type { AWS } from '@serverless/typescript';
import * as vars from './deployment/variables-staging.json';

const defaultRuntime = "nodejs18.x";

export const serverlessConfiguration: AWS = {
    service: "coke-battles",
    provider: {
        name: "aws",
        lambdaHashingVersion: "20201221",
        apiGateway: {
            shouldStartNameWithService: true
        },
        region: "us-east-2",
        stackName: vars.serverless_stack_name,
        tags: vars.tags,
        stackTags: {
            "stack": vars.serverless_stack_name
        },
        deploymentBucket: {
            name: vars.deployment_bucket,
            skipPolicySetup: true,
        } as any,
        environment: {
            DYNAMO_DB_MAIN_TABLE_NAME: vars.coke_battles_dynamodb_table_name
        },
        iamRoleStatements: [
            {
                Effect: "Allow",
                Action: [
                    "lambda:InvokeFunction",
                    "dynamodb:Query",
                    "dynamodb:Scan",
                    "dynamodb:GetItem",
                    "dynamodb:BatchGetItem",
                    "dynamodb:PutItem",
                    "dynamodb:UpdateItem",
                    "dynamodb:DeleteItem",
                    "dynamodb:ConditionCheckItem",
                ],
                Resource: "*"
            },
        ]
    },
    frameworkVersion: "3",
    plugins: [
        "serverless-offline",
        "serverless-webpack"
    ],
    functions: {
        createOrUpdateCokeBattle: {
            runtime: defaultRuntime,
            handler: "app/handlers.createOrUpdateCokeBattle",
            events: [
                {
                    http: {
                        authorizer: {
                            name: "authorizer"
                        },
                        method: "post",
                        path: "/battles",
                        integration: "lambda",
                        cors: true
                    }
                }
            ]
        },
        createOrUpdateCokeBattlesUser: {
            runtime: defaultRuntime,
            handler: "app/handlers.createOrUpdateCokeBattlesUser"
        },
        deleteCokeBattle: {
            runtime: defaultRuntime,
            handler: "app/handlers.deleteCokeBattle",
            events: [
                {
                    http: {
                        authorizer: {
                            name: "authorizer"
                        },
                        method: "delete",
                        path: "/battles",
                        integration: "lambda",
                        cors: true
                    }
                }
            ]
        },
        listCokeBattlesUsers: {
            runtime: defaultRuntime,
            handler: "app/handlers.listCokeBattlesUsers",
            events: [
                {
                    http: {
                        authorizer: {
                            name: "authorizer"
                        },
                        request: {
                            parameters: {
                                querystrings: {
                                    "searchString": false
                                }
                            }
                        },
                        method: "get",
                        path: "/users/list",
                        integration: "lambda",
                        cors: true
                    }
                }
            ]
        },
        listCokeBattles: {
            runtime: defaultRuntime,
            handler: "app/handlers.listCokeBattles",
            events: [
                {
                    http: {
                        request: {
                            parameters: {
                                querystrings: {
                                    "namespaceId": true
                                }
                            }
                        },
                        method: "get",
                        path: "/battles/list",
                        integration: "lambda",
                        cors: true
                    }
                }
            ]
        },
        getUserProfileFromAuth0ByToken: {
            runtime: defaultRuntime,
            handler: "app/handlers.getUserProfileFromAuth0",
            environment: {
                AUTH0_DOMAIN: vars.auth0_domain
            },
            events: [
                {
                    http: {
                        authorizer: {
                            name: "authorizer"
                        },
                        request: {
                            parameters: {
                                querystrings: {
                                    "accessToken": true
                                }
                            }
                        },
                        method: "get",
                        path: "/profile",
                        integration: "lambda",
                        cors: true
                    }
                }
            ]
        },
        getNamespace: {
            runtime: defaultRuntime,
            handler: "app/handlers.getNamespace",
            events: [{
                http: {
                    request: {
                        parameters: {
                            querystrings: {
                                "namespaceId": true
                            }
                        }
                    },
                    method: "get",
                    path: "/namespaces",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        createOrUpdateNamespace: {
            runtime: defaultRuntime,
            handler: "app/handlers.createOrUpdateNamespace",
            events: [{
                http: {
                    authorizer: {
                        name: "authorizer"
                    },
                    method: "post",
                    path: "/namespaces",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        addNormalUserToNamespace: {
            runtime: defaultRuntime,
            handler: "app/handlers.addNormalUserToNamespace",
            events: [{
                http: {
                    authorizer: {
                        name: "authorizer"
                    },
                    method: "post",
                    path: "/usersAndNamespaces/addUser",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        removeUserFromNamespace: {
            runtime: defaultRuntime,
            handler: "app/handlers.removeUserFromNamespace",
            events: [{
                http: {
                    authorizer: {
                        name: "authorizer"
                    },
                    method: "delete",
                    path: "/usersAndNamespaces",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        listUserNamespaces: {
            runtime: defaultRuntime,
            handler: "app/handlers.listUserNamespaces",
            events: [{
                http: {
                    authorizer: {
                        name: "authorizer"
                    },
                    method: "get",
                    path: "/usersAndNamespaces/",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        listNamespaceUsers: {
            runtime: defaultRuntime,
            handler: "app/handlers.listNamespaceUsers",
            events: [{
                http: {
                    authorizer: {
                        name: "authorizer"
                    },
                    method: "get",
                    path: "/usersAndNamespaces/users",
                    integration: "lambda",
                    cors: true
                }
            }]
        },
        authorizer: {
            runtime: defaultRuntime,
            handler: "app/authorizer.handler",
            environment: {
                AUTH0_DOMAIN: vars.auth0_domain,
                SAVE_USER_PROFILE_LAMBDA: { "Fn::GetAtt": ["CreateOrUpdateCokeBattlesUser" + "LambdaFunction", "Arn"] }
            }
        },
    },
    custom: {
        enterpriseDisabled: true,
        webpack: {
            webpackConfig: './webpack.config.js',
            includeModules: {
                forceExclude: [
                    'aws-sdk',
                    '@aws-sdk/client-lambda',
                    '@aws-sdk/client-dynamodb',
                    '@aws-sdk/lib-dynamodb',
                ]
            }
        },
    }
};

module.exports = serverlessConfiguration;
