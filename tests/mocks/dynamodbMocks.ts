import { DeleteItemCommandOutput, PutItemCommandOutput, QueryCommandOutput, ScanCommandOutput, TransactWriteItemsCommandOutput } from "@aws-sdk/client-dynamodb";
import { BatchGetCommandOutput, GetCommandOutput } from "@aws-sdk/lib-dynamodb";


export function mockDynamoGetResult<T extends object | null>(result: T): Promise<GetCommandOutput> {
    return Promise.resolve({ Item: result }) as any;
};
export function mockDynamoBatchGetResult(result: BatchGetCommandOutput['Responses']): Promise<BatchGetCommandOutput> {
    return Promise.resolve({ Responses: result }) as any;
};
export function mockDynamoQueryResult<T extends object>(result: T[]): Promise<QueryCommandOutput> {
    return Promise.resolve({ Items: result }) as any;
};

export function mockDynamoPutResult(): Promise<PutItemCommandOutput> {
    return Promise.resolve() as any;
};
export function mockDynamoTransactWriteResult(): Promise<TransactWriteItemsCommandOutput> {
    return Promise.resolve() as any;
};

export function mockDynamoDeleteResult(): Promise<DeleteItemCommandOutput> {
    return Promise.resolve() as any;
};

export function mockDynamoScanResult<T extends object>(result: T[], key?: ScanCommandOutput['LastEvaluatedKey']): Promise<ScanCommandOutput> {
    return Promise.resolve({ Items: result, LastEvaluatedKey: key }) as any;
};