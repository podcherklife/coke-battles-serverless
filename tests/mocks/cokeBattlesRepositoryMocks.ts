import { when } from "ts-mockito";
import { CokeBattlesRepository } from "../../app/persistence/CokeBattlesRepository";
import { CokeBattle } from "../../app/persistence/sharedTypes/CokeBattle";
import { NamespaceId } from "../../app/persistence/sharedTypes/Namespace";

export function withBattle(battle: CokeBattle, repository: CokeBattlesRepository) {
    when(repository.getBattleById(battle.namespace.id, battle.id)).thenReturn(Promise.resolve(battle));
}

export function withBattles(battles: CokeBattle[], namespaceId: NamespaceId, repository: CokeBattlesRepository) {
    when(repository.listBattles(namespaceId)).thenReturn(Promise.resolve(battles));
}