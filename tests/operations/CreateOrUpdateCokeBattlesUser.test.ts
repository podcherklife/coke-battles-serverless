import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import * as UUID from "uuid";
import { CreateOrUpdateCokeBattlesUser } from "../../app/operations/CreateOrUpdateCokeBattlesUser";
import { CreateOrUpdateCokeBattlesUsersTypes } from "../../app/operations/types/CreateOrUpdateCokeBattlesUser";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { BaseUser, RegisteredUser } from "../../app/persistence/sharedTypes/User";
import { withProfile } from "../usersRepositoryMocks";

describe(CreateOrUpdateCokeBattlesUser.name, () => {
    let usersRepoMock: UsersRepository;

    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        container.clearInstances();
        container.registerInstance(UsersRepository, instance(usersRepoMock));
        container.registerInstance("IdGenerator", UUID.v4);
    });

    const caller = {
        username: "username",
        id: "userId"
    }

    const callerProfile: RegisteredUser = {
        id: caller.id,
        username: caller.username,
        email: "callse@email",
        type: "human"
    };

    const testLambda = () => {
        const operation = container.resolve(CreateOrUpdateCokeBattlesUser);
        return lambdaTester(operation.createOrUpdateCokeBattlesUser.bind(operation));
    }

    it("should update profile if profile exists", () => {
        withProfile(callerProfile, usersRepoMock);
        when(usersRepoMock.saveUser(anything())).thenReturn(Promise.resolve());

        return testLambda()
            .event({
                body: CreateOrUpdateCokeBattlesUsersTypes.CreateOrUpdateUserInputSchema.parse(callerProfile)
            })
            .expectResolve((result: any) => {
                const expectedSavedProfile = callerProfile;
                expectSaveCalledOnceWithUserEqualTo(expectedSavedProfile);
                expectGetByEmailCalledOnceWithEmail(callerProfile.email!);
                expect(result).to.deep.equal(expectedSavedProfile);
            });
    });

    it("should create profile if profile does not exist", () => {
        when(usersRepoMock.getUserById(anything())).thenReturn(Promise.resolve(null));
        when(usersRepoMock.saveUser(anything())).thenReturn(Promise.resolve());

        const generatedUserId = "generatedUserId";
        container.register("IdGenerator", {
            useValue: () => generatedUserId
        });

        return testLambda()
            .event({
                body: CreateOrUpdateCokeBattlesUsersTypes.CreateOrUpdateUserInputSchema.parse(callerProfile)
            })
            .expectResolve((result: any) => {
                const expectedSavedProfile = {
                    ...callerProfile,
                    id: generatedUserId,
                };
                expectSaveCalledOnceWithUserEqualTo(expectedSavedProfile);
                expectGetByEmailCalledOnceWithEmail(callerProfile.email);
                expect(result).to.deep.equal(expectedSavedProfile);
            });
    });

    function expectSaveCalledOnceWithUserEqualTo(expectedUser: BaseUser) {
        const [savedUser] = capture(usersRepoMock.saveUser).first();
        expect(savedUser).to.deep.equal(expectedUser);
        verify(usersRepoMock.saveUser(anything())).once();
    }

    function expectGetByEmailCalledOnceWithEmail(email: string) {
        verify(usersRepoMock.getUserByEmail(email)).once();
    }

});