import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { ListCokeBattles } from "../../app/operations/ListCokeBattles";
import { ListCokeBattlesTypes } from "../../app/operations/types/ListCokeBattlesTypes";
import { CokeBattlesRepository } from "../../app/persistence/CokeBattlesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { CokeBattle } from "../../app/persistence/sharedTypes/CokeBattle";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { withBattles } from "../mocks/cokeBattlesRepositoryMocks";


describe(ListCokeBattles.name, () => {
    let cokeBattlesRepoMock: CokeBattlesRepository;
    let usersRepositoryMock: UsersRepository;

    beforeEach(() => {
        cokeBattlesRepoMock = mock(CokeBattlesRepository);
        usersRepositoryMock = mock(UsersRepository);
        container.clearInstances();
        container.registerInstance(CokeBattlesRepository, instance(cokeBattlesRepoMock));
        container.registerInstance(UsersRepository, instance(usersRepositoryMock));
    });

    const namespace = {
        id: "namespaceId"
    }

    const testLambda = () => {
        const operation = container.resolve(ListCokeBattles);
        return lambdaTester(operation.listCokeBattles.bind(operation));
    }

    it("should just work", () => {
        when(cokeBattlesRepoMock.listBattles(namespace.id)).thenReturn(Promise.resolve([]));

        return testLambda()
            .event({
                query: {
                    namespaceId: namespace.id
                }
            })
            .expectResolve((result: any) => {
                expect(result).to.deep.equal([]);
                verify(cokeBattlesRepoMock.listBattles(namespace.id)).called();
            });
    })

    it.skip("should throw error when namespaceId is empty", () => { // this should be tested on handler level
        return testLambda()
            .event({
                query: {
                    namespaceId: ""
                }
            })
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(Error);
                verify(cokeBattlesRepoMock.listBattles(anything())).never();
            })
    });

    describe("enrichment", () => {
        let participant: BaseUser = {
            id: "sampleParticipantId",
            username: "sampleParticipantName",
            email: "participantEmail",
            type: "human"
        }

        let creator: BaseUser = {
            id: "creatorId",
            username: "creatorUsername",
            email: "creatorEmail",
            type: "human"
        }

        let battle: CokeBattle = {
            id: "battleId",
            creationDate: 123,
            creator: { id: creator.id },
            description: "description",
            namespace: { id: namespace.id },
            sides: [{
                description: "side1Description",
                participants: [{ id: participant.id }]
            }],
            expectedResolutionDate: 999
        }
        beforeEach(() => {
            when(usersRepositoryMock.getByIds(anything())).thenReturn(Promise.resolve(new Map([[participant.id, participant], [creator.id, creator]])));
        })

        it("should return stored battles and enrich them with users", async () => {
            withBattles([battle], namespace.id, cokeBattlesRepoMock);
            return testLambda()
                .event({
                    query: {
                        namespaceId: namespace.id
                    }
                })
                .expectResolve((result: any) => {
                    expect(result).to.deep.equal([{
                        battleId: battle.id,
                        creationDate: battle.creationDate,
                        creator: {
                            userId: creator.id,
                            username: creator.username,
                        },
                        description: battle.description,
                        expectedResolutionDate: battle.expectedResolutionDate,
                        namespaceId: namespace.id,
                        sides: [{
                            description: battle.sides[0].description,
                            participants: [{
                                userId: participant.id,
                                username: participant.username,
                            }]
                        }],
                    } as ListCokeBattlesTypes.CokeBattle]);
                    verify(usersRepositoryMock.getByIds(anything())).atMost(1);
                })
        });


        it("should not enrich battles with users when no battles are stored", async () => {
            withBattles([], namespace.id, cokeBattlesRepoMock);
            return testLambda()
                .event({
                    query: {
                        namespaceId: namespace.id
                    }
                })
                .expectResolve((result: any) => {
                    expect(result).to.deep.equal([]);
                    verify(usersRepositoryMock.getByIds(anything())).never();
                })
        });
    })
});
