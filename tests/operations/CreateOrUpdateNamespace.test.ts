import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import * as UUID from "uuid";
import { CreateOrUpdateNamespace } from "../../app/operations/CreateOrUpdateNamespace";
import { ConditionCheckError } from "../../app/persistence/AbstractRepository";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../../app/persistence/UsersNamespacesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { AccessError, BadRequest } from "../../app/utils/handlerUtils";
import { withNamespace } from "../namespacesRepositoryMocks";
import { withUser, withUserRef } from "../usersRepositoryMocks";

describe(CreateOrUpdateNamespace.name, () => {
    let namespacesRepositoryMock: NamespacesRepository;
    let usersRepositoryMock: UsersRepository;
    let usersNamespacesRepositoryMock: UsersNamespacesRepository;

    beforeEach(() => {
        namespacesRepositoryMock = mock(NamespacesRepository);
        usersRepositoryMock = mock(UsersRepository);
        usersNamespacesRepositoryMock = mock(UsersNamespacesRepository);
        container.clearInstances();
        container.registerInstance(NamespacesRepository, instance(namespacesRepositoryMock));
        container.registerInstance(UsersRepository, instance(usersRepositoryMock));
        container.registerInstance(UsersNamespacesRepository, instance(usersNamespacesRepositoryMock));
        container.registerInstance("IdGenerator", UUID.v4)
    });

    const caller: BaseUser = {
        id: "callerId",
        email: "caller_email",
        type: "human",
        username: "caller_username"
    };

    const callersNamespace: Namespace = {
        id: "namespace",
        name: "some namespace",
        creator: caller
    };

    const testLambda = () => {
        const operation = container.resolve(CreateOrUpdateNamespace);
        return lambdaTester(operation.createOrUpdateNamespace.bind(operation));
    }

    beforeEach(() => {
        withUser(caller, usersRepositoryMock);
    });

    it("should update namespace as is", () => {
        withNamespace(callersNamespace, namespacesRepositoryMock);
        when(namespacesRepositoryMock.saveNamespace(anything())).thenReturn(Promise.resolve());

        return testLambda()
            .event({
                principalId: caller.id,
                body: {
                    namespaceId: callersNamespace.id,
                    name: callersNamespace.name,
                },
            })
            .expectResolve((result: any) => {
                const savedNamespace = capture(namespacesRepositoryMock.saveNamespace).first()[0];
                expect(savedNamespace).to.deep.equal({ id: callersNamespace.id, name: callersNamespace.name, creator: caller });
                verify(usersRepositoryMock.getUserById(caller.id)).times(1);
                verify(usersNamespacesRepositoryMock.saveUserInNamespace(anything())).never();
                expect(result).to.equal(callersNamespace.id);
            });
    });

    it("should fail if caller and creator are different", () => {
        withNamespace(callersNamespace, namespacesRepositoryMock);
        let someOtherUserId = caller.id + "_other_id";
        withUserRef({ id: someOtherUserId }, usersRepositoryMock);

        return testLambda()
            .event({
                principalId: someOtherUserId,
                body: {
                    namespaceId: callersNamespace.id,
                    name: callersNamespace.name,
                },
            })
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(AccessError);

                verify(usersRepositoryMock.getUserById(someOtherUserId)).once();
                verify(namespacesRepositoryMock.getNamespaceById(callersNamespace.id)).once();
                verify(usersNamespacesRepositoryMock.saveUserInNamespace(anything())).never();
            });
    });

    it("should fail if namespace id is incorrect", () => {
        return testLambda()
            .event({
                principalId: caller.id,
                body: {
                    namespaceId: "no_such_id_in_database",
                    name: callersNamespace.name,
                },
            })
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(BadRequest);

                verify(namespacesRepositoryMock.saveNamespace(anything())).never();
                verify(usersNamespacesRepositoryMock.saveUserInNamespace(anything())).never();
            });
    });

    it("should generate new id for new namespaces and add user-namespace link", () => {
        when(namespacesRepositoryMock.saveNamespace(anything())).thenReturn(Promise.resolve());

        const namespaceWithoutId = {
            name: callersNamespace.name
        };
        const generatedId = "generated_id";
        container.register("IdGenerator", {
            useValue: () => generatedId
        });
        return testLambda()
            .event({
                principalId: caller.id,
                body: namespaceWithoutId
            })
            .expectResolve((result: any) => {
                verify(namespacesRepositoryMock.getNamespaceById(anything())).never();
                verify(namespacesRepositoryMock.saveNamespace({
                    ...namespaceWithoutId,
                    creator: {
                        id: caller.id
                    },
                    id: generatedId
                }));
                verify(usersNamespacesRepositoryMock.saveUserInNamespace({
                    user: { id: caller.id },
                    namespace: { id: generatedId },
                    role: "admin",
                    displayName: caller.username
                }));
                expect(result).to.equal(generatedId);
            });
    });

    it("should throw BadRequest when updating namespace throws condition error", async () => {
        when(namespacesRepositoryMock.saveNamespace(anything())).thenReturn(Promise.reject(new ConditionCheckError("some message")));
        withNamespace(callersNamespace, namespacesRepositoryMock);

        let namespaceInput = [{
            name: "namespaceWithoutId",
        }, {
            name: callersNamespace.name,
            namespaceId: callersNamespace.id,
        }];

        return Promise.all(namespaceInput.map(e => ({
            body: e,
            principalId: caller.id
        })).map(event => {
            return testLambda()
                .event(event).expectReject((result: any) => {
                    expect(result).to.be.an.instanceOf(BadRequest);
                })
        }));

    })

});