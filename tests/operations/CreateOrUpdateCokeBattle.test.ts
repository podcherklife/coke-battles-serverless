import { expect } from 'chai';
import lambdaTester from 'lambda-tester';
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import * as UUID from "uuid";
import { CreateOrUpdateCokeBattle } from '../../app/operations/CreateOrUpdateCokeBattle';
import { CreateOrUpdateCokeBattleTypes } from '../../app/operations/types/CreateOrUpdateCokeBattleTypes';
import { CokeBattlesRepository } from '../../app/persistence/CokeBattlesRepository';
import { NamespacesRepository } from '../../app/persistence/NamespacesRepository';
import { UsersRepository } from '../../app/persistence/UsersRepository';
import { CokeBattle, CokeBattleSchema } from '../../app/persistence/sharedTypes/CokeBattle';
import { Namespace } from '../../app/persistence/sharedTypes/Namespace';
import { withBattle } from '../mocks/cokeBattlesRepositoryMocks';
import { withNamespace } from '../namespacesRepositoryMocks';
import { withUser } from '../usersRepositoryMocks';



describe(("createOrUpdateCokeBattle"), () => {
    let battlesRepoMock: CokeBattlesRepository;
    let usersRepoMock: UsersRepository;
    let namespacesRepoMock: NamespacesRepository;
    let defaultIdGenerator = UUID.v4;

    beforeEach(() => {
        battlesRepoMock = mock(CokeBattlesRepository);
        usersRepoMock = mock(UsersRepository);
        namespacesRepoMock = mock(NamespacesRepository);
        container.clearInstances();
        container.registerInstance(CokeBattlesRepository, instance(battlesRepoMock));
        container.registerInstance(UsersRepository, instance(usersRepoMock));
        container.registerInstance(NamespacesRepository, instance(namespacesRepoMock));
        container.registerInstance("IdGenerator", defaultIdGenerator);
    });
    const currentUser = {
        username: "username",
        id: "userId"
    }
    const participant = {
        username: currentUser.username,
        userId: "userId"
    }

    const savedNamespace: Namespace = {
        id: "namespaceId",
        name: "namespae",
        creator: currentUser
    };

    const battleInput = {
        battleId: "foo",
        description: "battle description",
        expectedResolutionDate: 234,
        sides: [{
            description: "fooSide",
            participants: [participant]
        }],
        namespaceId: savedNamespace.id
    } as CreateOrUpdateCokeBattleTypes.CokeBattleInput


    const savedBattleProjection = {
        ...battleInput,
        creator: currentUser,
        namespace: {
            id: battleInput.namespaceId,
        },
        creationDate: 123,
        id: battleInput.battleId,
        sides: battleInput.sides.map(e => ({ ...e, participants: e.participants.map(p => ({ username: "XXX", id: p.userId })) })),
    } as CokeBattle;
    beforeEach(() => {
        withUser({ ...currentUser, email: "email", type: "human" }, usersRepoMock);
    });

    const testLambda = () => {
        const operation = container.resolve(CreateOrUpdateCokeBattle);
        return lambdaTester(operation.createOrUpdateCokeBattle.bind(operation));
    }

    it("should update battle if it already exists", () => {
        withBattle(savedBattleProjection, battlesRepoMock);
        withNamespace(savedNamespace, namespacesRepoMock);
        when(battlesRepoMock.saveBattle(anything())).thenReturn(Promise.resolve());

        return testLambda()
            .event({
                principalId: currentUser.id,
                body: battleInput
            })
            .expectResolve((_: any) => {
                const [savedBattle] = capture(battlesRepoMock.saveBattle).last();

                expect(savedBattle).to.deep.equal(CokeBattleSchema.parse(savedBattleProjection));

                verify(battlesRepoMock.getBattleById(anything(), anything())).times(1);
                verify(battlesRepoMock.saveBattle(anything())).times(1);
                verify(usersRepoMock.getUserById(anything())).times(2);
                verify(namespacesRepoMock.getNamespaceById(savedNamespace.id));
            });

    });

    it('should create battle if none exists', () => {
        when(battlesRepoMock.saveBattle(anything())).thenReturn(Promise.resolve());
        withNamespace(savedNamespace, namespacesRepoMock);
        const generatedId = "generatedId";
        container.register("IdGenerator", {
            useValue: () => generatedId
        })

        return testLambda()
            .event({
                principalId: currentUser.id,
                body: {
                    ...battleInput,
                    battleId: undefined
                }
            })
            .expectResolve((_: any) => {
                const [savedBattle] = capture(battlesRepoMock.saveBattle).last();

                expect(Math.abs(savedBattle.creationDate - (new Date().getTime() / 1000))).to.be.lessThan(10);
                expect({
                    ...savedBattle,
                    creationDate: 0
                }).to.deep.equal(CokeBattleSchema.parse({
                    ...savedBattleProjection,
                    creationDate: 0,
                    id: generatedId,
                } as CokeBattle));

                verify(battlesRepoMock.getBattleById(anything(), anything())).never();
                verify(battlesRepoMock.saveBattle(anything())).times(1);
                verify(usersRepoMock.getUserById(anything())).times(2);
                verify(namespacesRepoMock.getNamespaceById(savedNamespace.id));
            });
    });

    it("should fail when adding battle to nonexistant namespace", () => {
        const nonExistantNamespaceId = "someNonExistantNamespace";
        return testLambda()
            .event({
                principalId: currentUser.id,
                body: {
                    ...battleInput,
                    namespace: {
                        id: nonExistantNamespaceId
                    }
                }
            })
            .expectReject((_: any) => {
                verify(battlesRepoMock.saveBattle(anything())).never();
                verify(namespacesRepoMock.getNamespaceById(nonExistantNamespaceId));
            });
    });
});