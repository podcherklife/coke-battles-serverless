import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anyString, deepEqual, instance, mock, strictEqual, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { ListNamespaceUsers } from "../../app/operations/ListNamespaceUsers";
import { ListNamespaceUsersTypes } from "../../app/operations/types/ListNamespaceUsersTypes";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../../app/persistence/UsersNamespacesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { UserParticipationInNamespace } from "../../app/persistence/sharedTypes/UserInNamespaceModel";
import { NotFound } from "../../app/utils/handlerUtils";

describe(ListNamespaceUsers.name, () => {

    let usersRepoMock: UsersRepository;
    let namespacesRepoMock: NamespacesRepository;
    let usersNamespacesRepoMock: UsersNamespacesRepository;

    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        namespacesRepoMock = mock(NamespacesRepository);
        usersNamespacesRepoMock = mock(UsersNamespacesRepository);
        container.clearInstances();
        container.registerInstance(UsersNamespacesRepository, instance(usersNamespacesRepoMock));
        container.registerInstance(NamespacesRepository, instance(namespacesRepoMock));
        container.registerInstance(UsersRepository, instance(usersRepoMock));

    });

    const testLambda = () => {
        return lambdaTester(ListNamespaceUsers.prototype.listNamespaceUsers.bind(container.resolve(ListNamespaceUsers)));
    }

    let caller: BaseUser = {
        id: "callerId",
        email: "callerEmail",
        type: "human",
        username: "callerUsername"
    };

    let sampleUser: BaseUser = {
        id: "sampleUserId",
        email: "sampleUserEmail",
        type: "human",
        username: "sampleUser"
    };

    let namespace: Namespace = {
        creator: caller,
        id: "ns1Id",
        name: "ns1"
    };

    let uns1: UserParticipationInNamespace = {
        namespace: namespace,
        role: "admin",
        user: caller,
        displayName: caller.username
    };

    let uns2: UserParticipationInNamespace = {
        namespace: namespace,
        role: "participant",
        user: sampleUser,
        displayName: sampleUser.username
    };



    describe(ListNamespaceUsers.prototype.listNamespaceUsers, () => {


        it("should return 404 if namespace id in invalid", async () => {
            return testLambda().event({
                query: {
                    namespaceId: "randomId"
                }
            }).expectReject((res: any) => {
                expect(res).to.be.an.instanceof(NotFound);
            });
        });

        it("should return list of users in namespace", async () => {
            when(usersNamespacesRepoMock.getNamespaceUsers(deepEqual({ id: namespace.id }))).thenReturn(Promise.resolve([uns1, uns2]));
            when(namespacesRepoMock.getNamespaceById(namespace.id)).thenReturn(Promise.resolve(namespace));

            return await testLambda().event({
                principalId: caller.id,
                query: {
                    namespaceId: namespace.id
                }
            }).expectResolve(res => {
                expect(res).to.deep.equal([
                    {
                        userId: caller.id,
                        namespaceId: namespace.id,
                        role: "admin",
                        username: caller.username
                    },
                    {
                        userId: sampleUser.id,
                        namespaceId: namespace.id,
                        role: "participant",
                        username: sampleUser.username
                    }
                ] as ListNamespaceUsersTypes.Output[number][]);
            });
        });

        it("should use searchstring if present", async () => {
            when(usersNamespacesRepoMock.searchNamespaceUsersByName(deepEqual({ id: namespace.id }), anyString())).thenReturn(Promise.resolve([uns1, uns2]));
            when(namespacesRepoMock.getNamespaceById(namespace.id)).thenReturn(Promise.resolve(namespace));

            return await testLambda().event({
                principalId: caller.id,
                query: {
                    namespaceId: namespace.id,
                    searchString: "foo"
                }
            }).expectResolve(res => {
                expect(res).to.deep.equal([
                    {
                        userId: caller.id,
                        namespaceId: namespace.id,
                        role: "admin",
                        username: caller.username
                    },
                    {
                        userId: sampleUser.id,
                        namespaceId: namespace.id,
                        role: "participant",
                        username: sampleUser.username
                    }
                ] as ListNamespaceUsersTypes.Output[number][]);
                verify(usersNamespacesRepoMock.searchNamespaceUsersByName(deepEqual({ id: namespace.id }), strictEqual("foo"))).called();
            });

        });
    });

});