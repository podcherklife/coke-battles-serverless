import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, deepEqual, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { ListUserNamespaces } from "../../app/operations/ListUserNamespaces";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../../app/persistence/UsersNamespacesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { UserParticipationInNamespace } from "../../app/persistence/sharedTypes/UserInNamespaceModel";
import { NotFound } from "../../app/utils/handlerUtils";
import { withNamespace } from "../namespacesRepositoryMocks";

describe(ListUserNamespaces.name, () => {

    let usersRepoMock: UsersRepository;
    let namespacesRepoMock: NamespacesRepository;
    let usersNamespacesRepoMock: UsersNamespacesRepository;

    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        namespacesRepoMock = mock(NamespacesRepository);
        usersNamespacesRepoMock = mock(UsersNamespacesRepository);
        container.clearInstances();
        container.registerInstance(UsersRepository, instance(usersRepoMock));
        container.registerInstance(NamespacesRepository, instance(namespacesRepoMock));
        container.registerInstance(UsersNamespacesRepository, instance(usersNamespacesRepoMock));
    });

    const testLambda = () => {
        return lambdaTester(ListUserNamespaces.prototype.listUserNamespaces.bind(container.resolve(ListUserNamespaces)))
    }

    let user: BaseUser = {
        email: "email",
        id: "userId",
        type: "human",
        username: "username"
    };

    it(`should throw ${NotFound} if user not found`, () => {
        when(usersRepoMock.getUserById(user.id)).thenReturn(Promise.resolve(null));

        return testLambda()
            .event({
                query: {
                    userId: user.id
                }
            })
            .expectReject((result: any) => {
                verify(usersRepoMock.getUserById(user.id)).once();
                verify(namespacesRepoMock.getNamespaceById(anything())).never();
                verify(usersNamespacesRepoMock.getUserNamespaces(anything())).never();

                expect(result).to.be.an.instanceOf(NotFound);
            });
    });


    it(`should list user namespaces`, () => {
        when(usersRepoMock.getUserById(user.id)).thenReturn(Promise.resolve(user));
        when(usersNamespacesRepoMock.getUserNamespaces(user)).thenReturn()

        let ns1: Namespace = {
            creator: user,
            id: "id1",
            name: "ns1"
        };
        let ns2: Namespace = {
            creator: user,
            id: "id2",
            name: "ns2"
        };

        let uns1: UserParticipationInNamespace = {
            user: user,
            namespace: ns1,
            role: "participant",
            displayName: user.username
        }

        let uns2: UserParticipationInNamespace = {
            user: user,
            namespace: ns2,
            role: "admin",
            displayName: user.username
        };

        [ns1, ns2].forEach(ns => withNamespace(ns, namespacesRepoMock));
        when(usersNamespacesRepoMock.getUserNamespaces(deepEqual({ id: user.id }))).thenReturn(Promise.resolve([uns1, uns2]));

        return testLambda()
            .event({ query: { userId: user.id } })
            .expectResolve((result): any => {
                expect(result).to.include.deep.members([
                    { name: ns1.name, userId: ns1.creator.id, namespaceId: ns1.id, role: "participant" },
                    { name: ns2.name, userId: ns2.creator.id, namespaceId: ns2.id, role: "admin" },
                ])
                verify(namespacesRepoMock.getNamespaceById(anything())).times(2);
            });
    });



});
