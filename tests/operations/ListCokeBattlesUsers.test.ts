
import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { ListCokeBattlesUsers } from "../../app/operations/ListCokeBattlesUsers";
import { UsersRepository } from "../../app/persistence/UsersRepository";

describe(ListCokeBattlesUsers.name, () => {

    let usersRepoMock: UsersRepository;
    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        container.clearInstances();
        container.registerInstance(UsersRepository, instance(usersRepoMock));
    });

    const testLambda = () => {
        return lambdaTester(ListCokeBattlesUsers.prototype.listCokeBattlesUsers.bind(container.resolve(ListCokeBattlesUsers)))
    }

    it("scan until get enough results", () => {
        when(usersRepoMock.scanForUsers(anything(), anything())).thenReturn(Promise.resolve({
            Items: [{} as any, {} as any, {} as any, {} as any, {} as any, {} as any, {} as any],
            LastEvaluatedKey: { id: "key" }
        }))

        return testLambda()
            .event({
                query: {
                    searchString: "foo"
                }
            })
            .expectResolve((result: any[]) => {
                verify(usersRepoMock.scanForUsers(anything(), anything())).times(1);
                expect(result).lengthOf(ListCokeBattlesUsers.DEFAULT_BATCH_SIZE);
            });
    });

    it("scan until last evaluated key is empty", () => {
        const scanResults = [
            { Items: [], LastEvaluatedKey: { id: "key" } },
            { Items: [] }
        ];
        when(usersRepoMock.scanForUsers(anything(), anything())).thenReturn(Promise.resolve(scanResults[0]), Promise.resolve(scanResults[1]));

        return testLambda()
            .event({
                query: {
                    searchString: "foo"
                }
            })
            .expectResolve((_: any) => {
                const [firstSearchString, firstKey] = capture(usersRepoMock.scanForUsers).first();
                expect(firstSearchString).to.equal("foo");
                expect(firstKey).to.be.undefined;

                const [secondSearchString, secondKey] = capture(usersRepoMock.scanForUsers).second();
                expect(secondSearchString).to.eq("foo");
                expect(secondKey).to.deep.equal({ id: "key" })

                verify(usersRepoMock.scanForUsers(anything(), anything())).times(2);
            });
    })

});
