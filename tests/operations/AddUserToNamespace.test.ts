import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { UserManagement } from "../../app/context/RoleManagenent";
import { AddNormalUserToNamespace } from "../../app/operations/AddNormalUserToNamespace";
import { NamespacesAggregatesRepository } from "../../app/persistence/NamespacesAggregatesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { AccessError, BadRequest } from "../../app/utils/handlerUtils";
import { withNamespaceAggregate } from "../namespaceAggregateMocks";
import { withUser } from "../usersRepositoryMocks";

class SimpleGraph {
    completitionPromise: Promise<unknown>;
    resolveCompletitionPromise: (value: any) => void;
    externalPromisesView: { [key: string]: Promise<any> };
    internalPromisesView: { [key: string]: Promise<any> } = {};
    sideEffects: { [key: string]: (currentVal: any) => void } = {};
    funcs: {
        [key: string]: (input: any) => Promise<any>
    } = {};
    constructor() {
        this.externalPromisesView = new Proxy(this.internalPromisesView, {
            get: (target, prop, reciever) => {
                const val = Reflect.get(target, prop, reciever);
                if (val == null) {
                    throw new Error(`Property '${prop.toString()}' is not defined on this graph`)
                } else {
                    return val;
                }
            }
        })
        this.completitionPromise = new Promise((resolve, _reject) => {
            this.resolveCompletitionPromise = resolve;
        });
    }

    setOne(key: string, func: (input: { [key: string]: Promise<any> }) => Promise<any>) {
        this.funcs[key] = func;
        this.internalPromisesView[key] = this.completitionPromise.then(_ => this.funcs[key](this.externalPromisesView));
    }

    update(key: string, func: (input: Promise<any>) => Promise<any>) {
        const oldFunc = this.funcs[key];
        this.funcs[key] = (_input: any) => {
            return func(oldFunc(this.externalPromisesView));
        };
    }

    addSideEffect(key: string, sideEffectFunc: (currentVal: any) => void) {
        this.sideEffects[key] = sideEffectFunc;
        const oldVal = this.internalPromisesView[key]
        this.internalPromisesView[key] = oldVal.then((val) => sideEffectFunc(val)).then(() => oldVal)
    }

    async complete() {
        const promises = Object.keys(this.funcs).map(key => this.funcs[key](this.externalPromisesView).then(e => [key, e]));
        this.resolveCompletitionPromise("ok!");
        return await Promise.all(promises).then(e => {
            const result: any = {};
            e.forEach(e => result[e[0]] = e[1])
            return result;
        });
    }

}

const Graphed = function () {
    var graph: {
        get: () => SimpleGraph
    } = {} as any;
    const instance = {
        init: function () {
            beforeEach(() => {
                const newInstance = new SimpleGraph();
                graph.get = () => newInstance;
            })
        },
        setOne(key: string, func: (input: { [key: string]: Promise<any> }) => Promise<any>) {
            let oldFunc: any;
            let oldView: any;
            beforeEach(() => {
                oldFunc = graph.get().funcs[key];
                oldView = graph.get().internalPromisesView[key];
                graph.get().setOne(key, func);
            });
            afterEach(() => {
                graph.get().funcs[key] = oldFunc;
                graph.get().internalPromisesView[key] = oldView;
            })
        },
        update(key: string, func: (input: Promise<any>) => Promise<any>) {
            let oldFunc: any;
            let oldView: any;
            beforeEach(() => {
                oldFunc = graph.get().funcs[key];
                oldView = graph.get().internalPromisesView[key];
                return graph.get().update(key, func);
            });
            afterEach(() => {
                graph.get().funcs[key] = oldFunc;
                graph.get().internalPromisesView[key] = oldView;
            })
        },
        updateSideEffect(key: string, func: (currentVal: any) => void) {
            let oldSideEffect: any;
            beforeEach(() => {
                oldSideEffect = graph.get().sideEffects[key];
                return graph.get().addSideEffect(key, func);
            });
            afterEach(() => {
                graph.get().sideEffects[key] = oldSideEffect;
            })
        },
        async complete() {
            return graph.get().complete();
        },
        prop: function (key: string, initialSideEffectFunc?: (val: any) => void) {
            return {
                get: () => graph.get().externalPromisesView[key].then(() => graph.get().internalPromisesView[key]),
                set: (func: (input: { [key: string]: Promise<any> }) => Promise<any>, sideEffectFunc?: (val: any) => void) => {
                    const result = instance.setOne(key, func);
                    instance.updateSideEffect(key, sideEffectFunc ?? initialSideEffectFunc ?? ((_: any) => void 0));
                    return result;
                },
                modify: (func: (input: Promise<any>) => Promise<any>) => {
                    return instance.update(key, func);
                }
            };
        },
        get: function () {
            return graph.get()
        }
    }
    return instance;
}

describe(AddNormalUserToNamespace.name, () => {
    let namespaceAggregatesRepoMock: NamespacesAggregatesRepository;
    let usersRepoMock: UsersRepository;

    beforeEach(() => {
        namespaceAggregatesRepoMock = mock(NamespacesAggregatesRepository);
        usersRepoMock = mock(UsersRepository);
        when(namespaceAggregatesRepoMock.save(anything())).thenReturn(Promise.resolve());

        container.clearInstances();
        container.registerInstance(NamespacesAggregatesRepository, instance(namespaceAggregatesRepoMock));
        container.registerInstance(UsersRepository, instance(usersRepoMock));
    });

    const testLambda = () => {
        const operation = container.resolve(AddNormalUserToNamespace);
        const lambda = operation.handle.bind(operation)
        return lambdaTester(lambda);
    }

    let graph = Graphed();
    graph.init();

    const targetUserIdNode = graph.prop("targetUserId");
    const targetNamespaceIdNode = graph.prop("targetNamspaceId");
    const callerIdNode = graph.prop("callerId");

    type EventType = Parameters<ReturnType<typeof testLambda>['event']>[0];
    let event: () => Promise<EventType> = () =>
        Promise.all([targetUserIdNode.get(), targetNamespaceIdNode.get(), callerIdNode.get()])
            .then(([targetUserId, targetNamespaceId, callerId]) => ({
                query: {
                    namespaceId: targetNamespaceId!,
                    userId: targetUserId!
                },
                principalId: callerId!
            }));

    describe("given some valid input", () => {
        targetNamespaceIdNode.set(async () => "RANDOM_NAMESPACE_ID");
        callerIdNode.set(async () => "RANDOM_CALLER_ID");
        targetUserIdNode.set(async () => "RANDOM_TARGET_USER_UD");

        describe("and caller existing", () => {
            const caller: BaseUser = {
                email: "email",
                id: "totally_random_caller_id",
                username: "username",
                type: "human"
            };
            const callerNode = graph.prop("caller");
            callerNode.set(async () => caller, caller => withUser(caller, usersRepoMock));
            callerIdNode.set(async () => callerNode.get().then(e => e.id));

            it("should fail if namespaceId is not valid", async () => {
                await graph.complete();
                const ev = await event();
                return testLambda()
                    .event(ev)
                    .expectReject(async (result: any) => {
                        expect(result).to.be.an.instanceOf(BadRequest);
                        verify(namespaceAggregatesRepoMock.getById(anything())).once();
                        expect(capture(namespaceAggregatesRepoMock.getById).first()[0]).to.deep.equal({ id: "RANDOM_NAMESPACE_ID" });
                    })
            });
            describe("and target user existing", () => {
                let targetUserNode = graph.prop("targetUser", (user) => withUser(user, usersRepoMock));
                let targetUser = (targetUserId: string) => ({
                    email: "targetUserEmail",
                    type: "dummy",
                    username: "targetUserUsername",
                    id: targetUserId
                } as BaseUser);
                targetUserNode.set(async () => targetUserIdNode.get().then(targetUser))
                let targetUserId = "targetUserId";
                targetUserIdNode.set(async () => targetUserId);

                describe("and namespace existing", () => {
                    const namespaceProp = graph.prop("namespace", namespace =>
                        withNamespaceAggregate(namespace, namespaceAggregatesRepoMock));
                    const targetNamespaceCreatorNode = graph.prop("targetNamespaceCreator", user => {
                        withUser(user, usersRepoMock)
                    });
                    targetNamespaceCreatorNode.set(async () => {
                        return ({
                            ...caller,
                            id: "sokme_guyFIXME"
                        })
                    });
                    const userRoleInNamespace = graph.prop("userRole");

                    namespaceProp.set(() => targetNamespaceCreatorNode.get().then((targetNamespaceCreator) => {
                        return new UserManagement.NamespaceAggregate({
                            namespace: { id: "namespaceId", name: "someNamespaceName", creator: targetNamespaceCreator },
                            usersAndRoles: new Map([])
                        })
                    }))

                    userRoleInNamespace.set(async () => "participant", role =>
                        callerIdNode.get().then(callerId =>
                            namespaceProp.get().then(namespace => {
                                if (role != null) {
                                    namespace.usersAndRoles.set(callerId, { role })
                                }
                            })
                        )
                    )

                    targetNamespaceIdNode.set(() => namespaceProp.get().then(n => {
                        return n.namespace.id
                    }));

                    describe("and created not by caller", () => {
                        targetNamespaceCreatorNode.set(async () => ({
                            ...caller,
                            id: "namespace_creator"
                        }));
                        it("should fail with access error", async () => {
                            await graph.complete();
                            return event().then(ev => testLambda()
                                .event(ev)
                                .expectReject((result: any) => {
                                    expect(result).to.be.an.instanceOf(AccessError);
                                    verify(usersRepoMock.getByIds(anything())).once();
                                    expect(capture(usersRepoMock.getByIds).first()[0]).to.deep.equal(new Set([caller.id, targetUserId]));
                                }));
                        });

                        describe("and user is admin in namespace", () => {
                            userRoleInNamespace.set(async () => "admin");

                            it("should succeed if target user exists", async () => {
                                await graph.complete();
                                return event().then(ev => testLambda()
                                    .event(ev)
                                    .expectResolve(async (_: any) => {
                                        verify(usersRepoMock.getByIds(anything())).once();
                                        expect(capture(usersRepoMock.getByIds).first()[0]).to.deep.equal(new Set([caller.id, targetUserId]));
                                        expect(capture(namespaceAggregatesRepoMock.save).first()[0]).to.equal(await namespaceProp.get());
                                    })
                                );
                            });
                        })
                    });

                    describe("and created by caller who no longer has any role", () => {
                        targetNamespaceCreatorNode.set(async () => callerNode.get());
                        targetUserIdNode.set(async () => callerIdNode.get());
                        userRoleInNamespace.set(async _ => null)
                        it("should succeed if target user exists", async () => {
                            await graph.complete();
                            return event().then(ev => testLambda()
                                .event(ev)
                                .expectResolve(async (_: any) => {
                                    verify(usersRepoMock.getByIds(anything())).once();
                                    expect(capture(usersRepoMock.getByIds).first()[0]).to.deep.equal(new Set(["totally_random_caller_id"]));
                                    expect(capture(namespaceAggregatesRepoMock.save).first()[0]).to.deep.equal(await namespaceProp.get());
                                })
                            );
                        });
                    })
                });
            });
        });
    });
});