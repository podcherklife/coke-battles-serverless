import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, instance, mock, verify } from "ts-mockito";
import { container } from "tsyringe";
import { GetNamespace } from "../../app/operations/GetNamespace";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { errorToHttpStatusConvertingHandler, queryMappers, validatingQueryHandler } from "../../app/utils/handlerUtils";
import { withNamespace } from "../namespacesRepositoryMocks";

describe(GetNamespace.name, () => {
    let namespacesRepositoryMock: NamespacesRepository;

    beforeEach(() => {
        namespacesRepositoryMock = mock(NamespacesRepository);
        container.clearInstances()
        container.registerInstance(NamespacesRepository, instance(namespacesRepositoryMock));
    });

    const namespace: Namespace = {
        id: "namespace",
        name: "some namespace",
        creator: {
            id: "creator"
        }
    };

    const testLambda = () => {
        const operation = container.resolve(GetNamespace);
        return lambdaTester(errorToHttpStatusConvertingHandler(validatingQueryHandler(operation.getNamespace.bind(operation), {
            namespaceId: queryMappers.require
        })));
    };
    it("should call repository and return namespace", () => {
        withNamespace(namespace, namespacesRepositoryMock);

        return testLambda()
            .event({
                query: {
                    namespaceId: namespace.id
                }
            })
            .expectResolve((result: any) => {
                expect(result).to.deep.equal({
                    namespaceId: namespace.id,
                    creator: { userId: namespace.creator.id },
                    name: namespace.name,
                });

                verify(namespacesRepositoryMock.getNamespaceById(namespace.id)).called();
            });
    });


    it("should return 404 if namespace is not found", () => {
        return testLambda()
            .event({ query: { namespaceId: namespace.id } })
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(Error);
                expect(result.message).to.contain("[404]");

                verify(namespacesRepositoryMock.getNamespaceById(namespace.id)).once();
            });
    });

    it("should fail if parameter is missing", () => {
        return testLambda()
            .event({
                query: {
                }
            } as any)
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(Error);
                expect(result.message).to.contain("[400]");

                verify(namespacesRepositoryMock.getNamespaceById(anything())).never()
            });
    });

});