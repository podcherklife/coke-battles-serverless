import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { UserManagement } from "../../app/context/RoleManagenent";
import { RemoveUserFromNamespace } from "../../app/operations/RemoveUserFromNamespace";
import { NamespacesAggregatesRepository } from "../../app/persistence/NamespacesAggregatesRepository";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { AccessError, BadRequest } from "../../app/utils/handlerUtils";
import { withNamespaceAggregate } from "../namespaceAggregateMocks";
import { withUserRef } from "../usersRepositoryMocks";

describe(RemoveUserFromNamespace.name, () => {
    let usersRepoMock: UsersRepository;
    let namespacesRepoMock: NamespacesAggregatesRepository;

    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        namespacesRepoMock = mock(NamespacesAggregatesRepository);

        container.clearInstances();
        container.registerInstance(UsersRepository, instance(usersRepoMock));
        container.registerInstance(NamespacesAggregatesRepository, instance(namespacesRepoMock));
    });

    const testLambda = () => lambdaTester(RemoveUserFromNamespace.prototype.removeUserFromNamespace.bind(container.resolve(RemoveUserFromNamespace)));

    const namespaceAggregate = new UserManagement.NamespaceAggregate({
        namespace: {
            creator: {
                id: "target_namespace_creator"
            },
            id: "target_namespace_id",
            name: "target_namespace_name"
        },
        usersAndRoles: new Map()
    });

    function givenValidInput() {
        return testLambda()
            .event({
                principalId: caller.id,
                query: {
                    namespaceId: namespaceAggregate.namespace.id,
                    userId: targetUser.id
                }
            });
    }

    const caller = {
        username: "caller_username",
        id: "caller_id"
    }


    const targetUser: BaseUser = {
        id: "random_user_id",
        username: "random_username",
        email: "random_email",
        type: "human"
    }

    it.skip("should fail in http request does not contain required parameters", () => { // should test this on handler level
        return testLambda()
            .event({ query: {} } as any).expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(AccessError);
                expectGetUsersNotCalled();
                expectGetNamespaceNotCalled();
            });
    });

    it("should fail if target user id is incorrect", () => {
        when(usersRepoMock.getByIds(anything())).thenReturn(Promise.resolve(new Map()));

        return givenValidInput().expectReject((result: any) => {
            expect(result).to.be.an.instanceOf(BadRequest);
            expectGetUsersCalled();
            expectGetNamespaceCalled();
        });
    });

    it("should fail if target namepsace id is incorrect", () => {
        withUserRef(caller, usersRepoMock);
        withUserRef(targetUser, usersRepoMock);
        when(namespacesRepoMock.getById(anything())).thenReturn(Promise.resolve(null));

        return givenValidInput().expectReject((result: any) => {
            expect(result).to.be.an.instanceOf(BadRequest);
            expectGetUsersCalled();
            expectGetNamespaceCalled();
        });
    });

    it("should fail it caller is not namespace creator", () => {
        withUserRef(caller, usersRepoMock);
        withUserRef(targetUser, usersRepoMock);
        withNamespaceAggregate(namespaceAggregate, namespacesRepoMock);

        return givenValidInput()
            .expectReject((result: any) => {
                expect(result).to.be.an.instanceOf(Error);
                expectGetUsersCalled();
                expectGetNamespaceCalled();
            });
    });

    it("should save changes if passedValues are correct", () => {
        withUserRef(targetUser, usersRepoMock);
        withUserRef(caller, usersRepoMock);
        withNamespaceAggregate(namespaceAggregate, namespacesRepoMock);
        namespaceAggregate.addUserParticipation({ id: targetUser.id, role: "participant", displayName: targetUser.username });
        namespaceAggregate.addUserParticipation({ id: caller.id, role: "admin", displayName: caller.username });

        return givenValidInput()
            .expectResolve((_: any) => {
                expectGetUsersCalled();
                expectGetNamespaceCalled();
            });
    });

    function expectGetUsersCalled() {
        verify(usersRepoMock.getByIds(anything())).once();
        expect(capture(usersRepoMock.getByIds).first()[0]).to.contain.keys([targetUser.id, caller.id]);
    }

    function expectGetUsersNotCalled() {
        verify(usersRepoMock.getByIds(anything())).never();
    }

    function expectGetNamespaceCalled() {
        verify(namespacesRepoMock.getById(anything())).once();
        expect(capture(namespacesRepoMock.getById).first()[0]).to.deep.equal({ id: namespaceAggregate.namespace.id });
    }

    function expectGetNamespaceNotCalled() {
        verify(namespacesRepoMock.getById(anything())).never();
    }

});
