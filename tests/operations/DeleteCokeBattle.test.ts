import { expect } from "chai";
import lambdaTester from "lambda-tester";
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { DeleteCokeBattle } from "../../app/operations/DeleteCokeBattle";
import { CokeBattlesRepository } from "../../app/persistence/CokeBattlesRepository";
import { CokeBattle } from "../../app/persistence/sharedTypes/CokeBattle";
import { withBattle } from "../mocks/cokeBattlesRepositoryMocks";

describe(DeleteCokeBattle.name, () => {

    let battlesRepoMock: CokeBattlesRepository;

    const caller = {
        username: "username",
        id: "userId"
    }

    const battleOwnedByCaller: CokeBattle = {
        description: "some description",
        creator: caller,
        id: "fooBattleId",
        creationDate: 111,
        sides: [],
        namespace: {
            id: "namespaceId"
        },
        expectedResolutionDate: null
    };

    const battlePk: [string, string] = [battleOwnedByCaller.namespace.id, battleOwnedByCaller.id];


    beforeEach(() => {
        battlesRepoMock = mock(CokeBattlesRepository);
        container.clearInstances();
        container.registerInstance(CokeBattlesRepository, instance(battlesRepoMock));
    });

    const testLambda = () => {
        const operation = container.resolve(DeleteCokeBattle);
        return lambdaTester(operation.deleteCokeBattle.bind(operation));
    }
    it("should deleteBattle it it exists and user has permissions", () => {
        withBattle(battleOwnedByCaller, battlesRepoMock);
        when(battlesRepoMock.deleteBattle(anything(), anything())).thenReturn(Promise.resolve());


        return testLambda()
            .event(generateDeleteEvent(caller.id, battleOwnedByCaller))
            .expectResolve((_: any) => {
                verify(battlesRepoMock.getBattleById(anything(), anything())).times(1);
                expect(capture(battlesRepoMock.getBattleById).first()).to.deep.equal(battlePk);

                verify(battlesRepoMock.deleteBattle(anything(), anything())).times(1);
                expect(capture(battlesRepoMock.deleteBattle).first()).to.deep.equal(battlePk);
            });
    });

    it("should fail if battle does not exist", () => {
        return testLambda()
            .event(generateDeleteEvent(caller.id, battleOwnedByCaller))
            .expectError((_: any) => {
                verify(battlesRepoMock.getBattleById(anything(), anything())).times(1);
                expect(capture(battlesRepoMock.getBattleById).first()).to.deep.equal(battlePk);

                verify(battlesRepoMock.deleteBattle(...battlePk)).never();
            });
    });

    it("should fail if user has not permissions", () => {
        const battleNotOwnedByCaller = { ...battleOwnedByCaller, creator: { ...battleOwnedByCaller.creator, id: "someOtherUserId" } };
        withBattle(battleNotOwnedByCaller, battlesRepoMock);

        return testLambda()
            .event(generateDeleteEvent(caller.id, battleOwnedByCaller))
            .expectError((_: any) => {
                verify(battlesRepoMock.getBattleById(anything(), anything())).times(1);
                expect(capture(battlesRepoMock.getBattleById).first()).to.deep.equal(battlePk);

                verify(battlesRepoMock.deleteBattle(anything(), anything())).never();
            });
    });
});

function generateDeleteEvent(callerId: string, battlePk: { namespace: { id: string }, id: string }) {
    return {
        principalId: callerId,
        body: {
            battleId: battlePk.id,
            namespaceId: battlePk.namespace.id,
        }
    };
}