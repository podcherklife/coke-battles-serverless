
import { expect } from "chai";
import lambdaTester from "lambda-tester";
import request from 'request';
import { RequestPromise, RequestPromiseAPI, RequestPromiseOptions } from 'request-promise';
import { anything, capture, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { GetUserProfileFromAuth0ByToken } from "../../app/operations/GetUserProfileFromAuth0ByToken";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { withUser } from "../usersRepositoryMocks";

describe(GetUserProfileFromAuth0ByToken, () => {
    let usersRepoMock: UsersRepository;
    let mockedFetch: GetUserProfileFromAuth0ByToken['fetch']

    const caller = {
        username: "username",
        id: "userId",
    }

    const callerProfileOnAuth0 = {
        picture: "picture"
    }


    beforeEach(() => {
        usersRepoMock = mock(UsersRepository);
        mockedFetch = mock<RequestPromiseAPI>();
        container.clearInstances();
        container.registerInstance(UsersRepository, instance(usersRepoMock));
        container.registerInstance("Fetch", instance(mockedFetch).apply);
        container.registerInstance("Auth0Domain", "someValue");
    });

    const testLambda = () => {
        const operation = container.resolve(GetUserProfileFromAuth0ByToken);
        return lambdaTester(operation.getProfile.bind(operation));
    }

    it("should just work", () => {
        withUser({ ...caller, email: "email", type: "human" }, usersRepoMock);

        when(mockedFetch.apply(anything() as unknown as RequestPromiseOptions & request.UriOptions))
            .thenReturn({
                then: (callback: (input: any) => void) => callback(callerProfileOnAuth0)
            } as RequestPromise<any>);

        return testLambda()
            .event({
                query: {
                    accessToken: "foo"
                },
                principalId: caller.id
            })
            .expectResolve((result: any) => {
                const [userId] = capture(usersRepoMock.getUserById).first();
                expect(userId).to.equal(caller.id);
                expect(result).to.deep.equal({
                    ...caller,
                    picture: "picture"
                })

                verify(mockedFetch.apply(anything())).once();
                verify(usersRepoMock.getUserById(anything())).once();
            });
    })

});