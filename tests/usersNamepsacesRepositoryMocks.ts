import { deepEqual, when } from "ts-mockito";
import { UsersNamespacesRepository } from "../app/persistence/UsersNamespacesRepository";
import { UserParticipationInNamespace } from "../app/persistence/sharedTypes/UserInNamespaceModel";

export function withUserInNamespace(userInNamespace: UserParticipationInNamespace, repository: UsersNamespacesRepository) {
    when(repository.getUserInNamespace(deepEqual({ namespaceId: userInNamespace.namespace.id, userId: userInNamespace.user.id })))
        .thenReturn(Promise.resolve(userInNamespace));
}
