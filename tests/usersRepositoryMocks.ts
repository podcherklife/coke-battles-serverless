import { anything, when } from "ts-mockito";
import { UsersRepository } from "../app/persistence/UsersRepository";
import { BaseUser, RegisteredUser, UserRef } from "../app/persistence/sharedTypes/User";


export function withUser(user: BaseUser, repository: UsersRepository & { knownIdsMap?: any }) {
    when(repository.getUserById(user.id)).thenReturn(Promise.resolve({
        actualId: user.id,
        id: user.id,
        email: user.email,
        username: user.username,
        type: user.type
    }));
    if ("knownIdsMap" in repository) {
        const knownIdsMap: Map<BaseUser['id'], BaseUser> = repository["knownIdsMap"];
        knownIdsMap.set(user.id, user);
    } else {
        const knownIdsMap = new Map();
        Object.defineProperty(repository, 'knownIdsMap', {
            get: function () {
                return knownIdsMap;
            }
        });
        knownIdsMap.set(user.id, user);
        when(repository.getByIds(anything())).
            thenCall(async (ids: Set<string>) => {
                const resultMap = new Map();
                Array.from(ids).map(id => knownIdsMap.get(id)).filter(e => e != null).forEach(e => resultMap.set(e!.id, e));
                return Promise.resolve(resultMap);
            })
    }
}

export function withUserRef(userRef: UserRef, repository: UsersRepository) {
    return withUser({
        id: userRef.id,
        email: `email_${userRef.id}`,
        username: `username_${userRef}`,
        type: "human"
    }, repository);
}

export function withProfile(profile: RegisteredUser, repository: UsersRepository) {
    when(repository.getUserByEmail(profile.email)).thenReturn(Promise.resolve({
        ...profile,
    }));

    when(repository.getUserById(profile.id)).thenReturn(Promise.resolve({
        ...profile,
    }));
}