import { expect } from 'chai';
import lambdaTester from 'lambda-tester';
import { AccessError, BadRequest, errorToHttpStatusConvertingHandler, NotFound, queryMappers, validatingQueryHandler } from '../../app/utils/handlerUtils';

describe('handlerUtils', () => {
    describe(validatingQueryHandler.name, () => {
        const handler = async (event: { query: { num: number, str: string }, principalId?: string }) => {
            if (typeof event.query.num !== "number") {
                throw new Error("wrong runtime argument type");
            }
            return `${event.query.num}|${event.query.str}|${event.principalId}`;
        }

        const typedHandler = validatingQueryHandler(handler, {
            num: queryMappers.toInt,
            str: queryMappers.pass
        });

        it('should parse and pass value futher', () => {
            return lambdaTester(typedHandler).event({ query: { num: "100", str: "bar" }, principalId: "fooUser" })
                .expectResolve((result: string) => {
                    expect(result).to.equal("100|bar|fooUser")
                });
        });

        it('should fail on incorrect input', () => {
            return lambdaTester(typedHandler).event({ query: { num: "foo", str: "bar" } })
                .expectReject((result: any) => {
                    expect(result).to.be.an('error')
                    expect(result.message).to.contain("Failed to convert")
                });
        });
    });

    describe(errorToHttpStatusConvertingHandler.name, () => {
        let consoleErrOld: any;
        beforeEach(() => {
            consoleErrOld = console.error;
            console.error = () => void 0;
        });
        afterEach(() => {
            console.error = consoleErrOld;
        });
        it('should replace error message with "[500]"', () => {
            var handler = errorToHttpStatusConvertingHandler(async () => { throw new Error("Nooo!") });

            return lambdaTester(handler).event({})
                .expectReject((result: any) => {
                    expect(result).to.have.property("message", "[500]");
                });
        });

        it(`should generate 403 message for ${AccessError.name} exceptions`, () => {
            var handler = errorToHttpStatusConvertingHandler(async () => { throw new AccessError("Nooo!") });

            return lambdaTester(handler).event({})
                .expectReject((result: any) => {
                    expect(result).to.have.property("message").that.contains("[403]");
                });
        });

        it(`should generate 404 message for ${NotFound.name} exceptions`, () => {
            var handler = errorToHttpStatusConvertingHandler(async () => { throw new NotFound("Nooo!") });

            return lambdaTester(handler).event({})
                .expectReject((result: any) => {
                    expect(result).to.have.property("message").that.contains("[404]");
                });
        });

        it(`should generate 401 message for ${BadRequest.name} exceptions`, () => {
            var handler = errorToHttpStatusConvertingHandler(async () => { throw new BadRequest("Nooo!") });

            return lambdaTester(handler).event({})
                .expectReject((result: any) => {
                    expect(result).to.have.property("message").that.contains("[400]");
                });
        });
    });

});