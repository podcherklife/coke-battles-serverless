import { deepEqual, when } from "ts-mockito";
import { UserManagement } from "../app/context/RoleManagenent";
import { NamespacesAggregatesRepository } from "../app/persistence/NamespacesAggregatesRepository";

export function withNamespaceAggregate(
    aggregate: UserManagement.NamespaceAggregate,
    repo: NamespacesAggregatesRepository,
) {
    when(repo.getById(deepEqual({ id: aggregate.namespace.id }))).thenReturn(Promise.resolve(aggregate))

}