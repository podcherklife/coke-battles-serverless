import { DynamoDBDocument, GetCommandInput, PutCommandInput, TransactWriteCommandInput } from "@aws-sdk/lib-dynamodb";
import { expect } from "chai";
import { anything, capture, instance, mock, when } from "ts-mockito";
import { container } from "tsyringe";
import { DynamoDbPropertiesInjectionToken } from "../../app/persistence/DynamoDbProperties";
import { UsersNamespacesRepository } from "../../app/persistence/UsersNamespacesRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { UserParticipationInNamespace } from "../../app/persistence/sharedTypes/UserInNamespaceModel";
import { mockDynamoGetResult, mockDynamoQueryResult, mockDynamoTransactWriteResult } from "../mocks/dynamodbMocks";

describe(UsersNamespacesRepository.name, () => {
    const tableName = "tableName";
    let dynamoDbMock: DynamoDBDocument;
    let repository: UsersNamespacesRepository;

    beforeEach(() => {
        dynamoDbMock = mock(DynamoDBDocument);
        container.clearInstances();
        container.registerInstance(DynamoDBDocument as any, instance(dynamoDbMock));
        container.registerInstance(DynamoDbPropertiesInjectionToken, { mainTableName: tableName });
        repository = container.resolve(UsersNamespacesRepository);
    });

    const sampleNamespace: Namespace = {
        id: "sampleNamespaceId",
        name: "sampleNamespaceName",
        creator: {
            id: "sampleNamespaceCreator"
        }
    }

    const sampleUser: BaseUser = {
        email: "email",
        id: "id",
        username: "username",
        type: "human"
    }

    const sampleUserNamespace: UserParticipationInNamespace = {
        namespace: {
            id: sampleNamespace.id
        },
        user: {
            id: sampleUser.id
        },
        role: "participant",
        displayName: sampleUser.username,
    };

    describe(UsersNamespacesRepository.prototype.getUserInNamespace.name, () => {
        it("should generate a proper input and return item from dynamodb", async () => {
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(sampleUserNamespace));

            const result = await repository.getUserInNamespace({
                namespaceId: sampleNamespace.id,
                userId: sampleUser.id
            });

            expect(result).to.deep.equal(sampleUserNamespace);
            expect(capture(dynamoDbMock.get).first()[0]).to.deep.equal({
                TableName: tableName,
                Key: {
                    pk: `namespace#${sampleNamespace.id}`,
                    sk: `user#${sampleUser.id}`
                }
            } as GetCommandInput)
        });
    });

    describe(UsersNamespacesRepository.prototype.getUserNamespaces.name, () => {
        it("should generate a proper input", async () => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([{ ...sampleUserNamespace, "garbage property": "garbage value" }]));

            const result = await repository.getUserNamespaces({ id: "sampleUserId" });

            expect(result).to.deep.equal([sampleUserNamespace]);

            const scanInput = capture(dynamoDbMock.query).first()[0];
            expect(scanInput).to.deep.equal({
                IndexName: "pk1-sk1-index",
                ExpressionAttributeValues: {
                    ":user_pk1": "user_in_namespace_by_user_id#sampleUserId"
                },
                TableName: tableName,
                Limit: 10,
                KeyConditionExpression: "pk1 = :user_pk1"
            });
        });
    });

    describe(UsersNamespacesRepository.prototype.getNamespaceUsers.name, () => {
        it("should generate a proper query and return items from dynamodb", async () => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([{ ...sampleUserNamespace, "garbage property": "garbage value" }]));

            const result = await repository.getNamespaceUsers({ id: sampleNamespace.id })

            expect(result).to.deep.equal([sampleUserNamespace]);

            const query = capture(dynamoDbMock.query).first()[0];
            expect(query).to.deep.equal({
                TableName: tableName,
                ExpressionAttributeValues: {
                    ":user_namespace_pk_value": `namespace#${sampleNamespace.id}`,
                    ":sk_prefix_value": "user#"
                },
                Limit: 10,
                KeyConditionExpression: "pk = :user_namespace_pk_value and begins_with(sk, :sk_prefix_value)"
            });
        });
    });

    describe(UsersNamespacesRepository.prototype.searchNamespaceUsersByName.name, () => {
        it("should generate a proper query and return items from dynamodb", async () => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([{ ...sampleUserNamespace, "garbage property": "garbage value" }]));

            const result = await repository.searchNamespaceUsersByName({ id: sampleNamespace.id }, "foo")

            expect(result).to.deep.equal([sampleUserNamespace]);

            const query = capture(dynamoDbMock.query).first()[0];
            expect(query).to.deep.equal({
                TableName: tableName,
                ExpressionAttributeValues: {
                    ":user_namespace_pk_value": `namespace#${sampleNamespace.id}`,
                    ":lsi1_prefix_value": `user_in_namespace_by_name#${"foo"}`
                },
                Limit: 10,
                KeyConditionExpression: "pk = :user_namespace_pk_value and begins_with(lsi1, :lsi1_prefix_value)"
            });
        });
    });

    describe(UsersNamespacesRepository.prototype.saveUserInNamespace.name, () => {
        it("should generate a proper input", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(null));

            await repository.saveUserInNamespace(sampleUserNamespace);

            expect(capture(dynamoDbMock.transactWrite).first()[0].TransactItems![0].Put).to.deep.equal({
                ConditionExpression: "attribute_not_exists(pk)",
                TableName: tableName,
                Item: {
                    ...sampleUserNamespace,
                    pk: `namespace#${sampleNamespace.id}`,
                    sk: `user#${sampleUser.id}`,
                    pk1: `user_in_namespace_by_user_id#${sampleUser.id}`,
                    sk1: `namespace#${sampleNamespace.id}`,
                    lsi1: `user_in_namespace_by_name#${sampleUser.username}`,
                    entityType: "user_in_namespace"
                }
            } as PutCommandInput)
        });
    });

    describe(UsersNamespacesRepository.prototype.deleteUserFromNamespace.name, () => {
        it("should generate a proper input", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(sampleUserNamespace));

            await repository.deleteUserFromNamespace({
                namespaceId: sampleNamespace.id,
                userId: sampleUser.id
            });

            expect(capture(dynamoDbMock.transactWrite).first()[0]).to.deep.equal({
                TransactItems: [{
                    Delete: {
                        TableName: tableName,
                        Key: {
                            pk: `namespace#${sampleNamespace.id}`,
                            sk: `user#${sampleUser.id}`,
                        }
                    },
                }]
            } as TransactWriteCommandInput)
        });
    });
});

