import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { expect } from "chai";
import { anything, capture, deepEqual, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { CokeBattlesRepository } from "../../app/persistence/CokeBattlesRepository";
import { DynamoDbProperties } from "../../app/persistence/DynamoDbProperties";
import { EntityType, Lsi1, Lsi2, PK } from "../../app/persistence/indices";
import { CokeBattle, SimpleUser } from "../../app/persistence/sharedTypes/CokeBattle";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { mockDynamoDeleteResult, mockDynamoGetResult, mockDynamoQueryResult, mockDynamoTransactWriteResult } from "../mocks/dynamodbMocks";


describe(CokeBattlesRepository.name, () => {
    let dynamoDbMock: DynamoDBDocument;
    let dynamoDbProperties: DynamoDbProperties = {
        mainTableName: "SampleTable"
    };
    let repository: CokeBattlesRepository;

    beforeEach(() => {
        container.clearInstances()
        dynamoDbMock = mock(DynamoDBDocument);
        container.register(DynamoDBDocument as any, {
            useValue: instance(dynamoDbMock)
        });
        container.register("DynamoDbProperties", {
            useValue: dynamoDbProperties
        })
        repository = container.resolve(CokeBattlesRepository);
    });

    const sampleCokeBattle: CokeBattle = {
        id: "foo",
        creationDate: 1,
        creator: {
            id: "1",
        },
        sides: [],
        description: "description",
        namespace: {
            id: "namespace"
        },
        expectedResolutionDate: null
    };

    const sampleUser: BaseUser = {
        email: "email",
        id: sampleCokeBattle.creator.id,
        username: "username",
        type: "human"
    }

    describe(CokeBattlesRepository.prototype.getBattleById.name, () => {

        it("pass id to database and unwrap result", async () => {
            const dynamoDbGetInput = deepEqual({
                TableName: dynamoDbProperties.mainTableName,
                Key: {
                    "pk": `namespace#${sampleCokeBattle.namespace.id}`,
                    "sk": `battle#${sampleCokeBattle.id}`
                }
            });
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(sampleCokeBattle));

            const result = await repository.getBattleById(sampleCokeBattle.namespace.id, sampleCokeBattle.id)

            expect(result).to.deep.equal({
                ...sampleCokeBattle,
                creator: {
                    id: sampleUser.id,
                } as SimpleUser
            });
            verify(dynamoDbMock.get(dynamoDbGetInput)).called();
        })
    });

    describe(CokeBattlesRepository.prototype.saveBattle.name, () => {

        it("should strip excess properties before saving", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult())

            const cokeBattleWithExcessProperties = {
                ...sampleCokeBattle,
            };

            const additionalProperty = "additionalProperty";
            (cokeBattleWithExcessProperties as any)[additionalProperty] = "foo";

            await repository.saveBattle(cokeBattleWithExcessProperties);

            expect(capture(dynamoDbMock.transactWrite).first()[0].TransactItems![1].Put!.Item).to.not.have.property(additionalProperty);

        });

        it("should generate expected putInput", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult())

            await repository.saveBattle(sampleCokeBattle);

            verify(dynamoDbMock.transactWrite(anything())).called();
            const [transactWriteInput] = capture(dynamoDbMock.transactWrite).first();
            expect(transactWriteInput.TransactItems![1].Put).to.deep.equal({
                Item: {
                    ...sampleCokeBattle,
                    entityType: "battle",
                    lsi1: `battle_by_creation_date#${sampleCokeBattle.creationDate}`,
                    lsi2: `battle_by_resolution_date#${sampleCokeBattle.expectedResolutionDate}`,
                    pk: `namespace#${sampleCokeBattle.namespace.id}`,
                    sk: `battle#${sampleCokeBattle.id}`,
                    namespace: {
                        id: "namespace"
                    }
                } as CokeBattle & Lsi1 & Lsi2 & PK & EntityType,
                TableName: dynamoDbProperties.mainTableName
            });
        });
    });

    describe(CokeBattlesRepository.prototype.deleteBattle.name, () => {
        it("should delete battle from dynamoDb", async () => {
            when(dynamoDbMock.delete(anything())).thenReturn(mockDynamoDeleteResult());

            await repository.deleteBattle(sampleCokeBattle.namespace.id, sampleCokeBattle.id);

            verify(dynamoDbMock.delete(deepEqual({
                TableName: dynamoDbProperties.mainTableName,
                Key: {
                    "pk": `namespace#${sampleCokeBattle.namespace.id}`,
                    "sk": `battle#${sampleCokeBattle.id}`
                }
            }))).called();
        });
    });


    describe(CokeBattlesRepository.prototype.listBattles.name, () => {
        const storedBattles: CokeBattle[] = [
            {
                id: "foo1",
                creationDate: new Date().getTime(),
                creator: sampleUser,
                sides: [{
                    description: "side description",
                    participants: [sampleUser]
                }],
                description: "description",
                expectedResolutionDate: new Date().getTime(),
                namespace: {
                    id: "namespace"
                }
            }
        ]

        it("should generate proper query", async () => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([]));

            const result = await repository.listBattles(storedBattles[0].namespace.id);

            expect(result).to.deep.equal([]);
            expect(capture(dynamoDbMock.query).first()[0]).to.deep.equal({
                TableName: dynamoDbProperties.mainTableName,
                Limit: 10,
                ExpressionAttributeValues: {
                    ":battle_sk_prefix": "battle#",
                    ":namespace": "namespace#namespace",
                },
                KeyConditionExpression: "pk = :namespace and begins_with(sk, :battle_sk_prefix)",
            });
        });

    })
});