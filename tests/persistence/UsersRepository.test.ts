import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { expect } from "chai";
import { anything, capture, deepEqual, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { DynamoDbPropertiesInjectionToken } from "../../app/persistence/DynamoDbProperties";
import { EntityType, Lsi1, Lsi1Name, Lsi2, Lsi2Name, PK } from "../../app/persistence/indices";
import { UsersRepository } from "../../app/persistence/UsersRepository";
import { BaseUser } from "../../app/persistence/sharedTypes/User";
import { mockDynamoBatchGetResult, mockDynamoPutResult, mockDynamoQueryResult } from "../mocks/dynamodbMocks";


describe(UsersRepository.name, () => {
    let tableName = "tableName";
    let dynamoDbMock: DynamoDBDocument;
    let repository: UsersRepository;

    beforeEach(() => {
        dynamoDbMock = mock(DynamoDBDocument);
        container.clearInstances();
        container.registerInstance(DynamoDBDocument as any, instance(dynamoDbMock));
        container.registerInstance(DynamoDbPropertiesInjectionToken, { mainTableName: tableName });
        repository = container.resolve(UsersRepository);
    });

    const sampleUser: BaseUser = {
        id: "userId",
        email: "email",
        username: "username",
        type: "human"
    };

    describe(UsersRepository.prototype.scanForUsers.name, () => {
        it("should generate a proper input", async () => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([sampleUser]));
            const key = { "id": "bar" };

            const result = await repository.scanForUsers("foo", key);

            expect(result.Items).to.deep.equal([sampleUser]);

            const scanInput = capture(dynamoDbMock.query).first()[0];
            expect(scanInput).to.deep.equal({
                IndexName: Lsi1Name,
                ExpressionAttributeValues: {
                    ":searchString": `user_by_name#foo`,
                    ":user_pk": "user"
                },
                ExpressionAttributeNames: {
                    "#user_by_name": "lsi1"
                },
                ExclusiveStartKey: key,
                TableName: tableName,
                Limit: UsersRepository.SCAN_SIZE,
                KeyConditionExpression: "pk = :user_pk and begins_with(#user_by_name, :searchString)"
            });
        });
    });

    describe(UsersRepository.prototype.saveUser.name, () => {
        it("should strip excess properties before saving", async () => {
            when(dynamoDbMock.put(anything())).thenReturn(mockDynamoPutResult())

            const userWithExcessProperties = {
                ...sampleUser,
            };

            const additionalProperty = "additionalProperty";
            (userWithExcessProperties as any)[additionalProperty] = "foo";

            await repository.saveUser(userWithExcessProperties);

            expect(capture(dynamoDbMock.put).first()[0].Item).to.not.have.property(additionalProperty);

        });


        it("should generate a proper putInput", async () => {
            when(dynamoDbMock.put(anything())).thenReturn(mockDynamoPutResult());

            await repository.saveUser(sampleUser);

            const [firstPut] = capture(dynamoDbMock.put).first();
            expect(firstPut).to.deep.equal({
                TableName: tableName,
                Item: {
                    ...sampleUser,
                    lsi1: `user_by_name#${sampleUser.username}`,
                    lsi2: `user_by_email#${sampleUser.email}`,
                    pk: "user",
                    sk: `user#${sampleUser.id}`,
                    entityType: "user",
                } as BaseUser & Lsi1 & Lsi2 & PK & EntityType,
            })
        });
    });

    describe("getById|getByEmail", () => {

        beforeEach(() => {
            when(dynamoDbMock.query(anything())).thenReturn(mockDynamoQueryResult([sampleUser]));
        });

        it("should return user from db by id", async () => {
            const result = await repository.getUserById(sampleUser.id);

            expect(result).to.deep.equal(sampleUser);
            verify(dynamoDbMock.query(deepEqual({
                TableName: tableName,
                KeyConditionExpression: `pk = :user_pk and sk = :userId`,
                ExpressionAttributeValues: {
                    ":userId": `user#${sampleUser.id}`,
                    ":user_pk": "user"
                }
            }))).called();
        })

        it("should return user from db by email", async () => {
            const result = await repository.getUserByEmail(sampleUser.email!);

            expect(result).to.deep.equal(sampleUser);
            verify(dynamoDbMock.query(deepEqual({
                TableName: tableName,
                IndexName: Lsi2Name,
                KeyConditionExpression: `pk = :user_pk and #user_by_email_key = :user_by_email_value`,
                ExpressionAttributeValues: {
                    ":user_by_email_value": `user_by_email#${sampleUser.email!.toLowerCase()}`,
                    ":user_pk": "user"
                },
                ExpressionAttributeNames: {
                    "#user_by_email_key": "lsi2"
                }
            }))).called();
        })
    });

    describe(UsersRepository.prototype.getByIds, () => {

        it("should do nothing and return empty list if list of ids is empty", async () => {
            const result = await repository.getByIds(new Set());

            expect(result).to.be.empty;
            verify(dynamoDbMock.batchGet(anything())).never();
        });

        it("should search database and return users", async () => {
            when(dynamoDbMock.batchGet(anything())).thenReturn(mockDynamoBatchGetResult({
                [tableName]: [sampleUser]
            }));

            const result = await repository.getByIds(new Set([sampleUser.id]));

            expect(result).to.have.lengthOf(1);
            expect(result.get(sampleUser.id)).to.deep.equal(sampleUser);
            expect(capture(dynamoDbMock.batchGet).first()[0]).to.deep.equal({
                RequestItems: {
                    [tableName]: {
                        Keys:
                            [{
                                "pk": "user",
                                "sk": "user#" + sampleUser.id
                            }]
                    }
                }
            });
        });
    });
});