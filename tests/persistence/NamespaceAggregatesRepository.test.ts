import { expect } from "chai";
import { capture, deepEqual, instance, mock, verify, when } from "ts-mockito";
import { container } from "tsyringe";
import { UserManagement } from "../../app/context/RoleManagenent";
import { NamespacesAggregatesRepository } from "../../app/persistence/NamespacesAggregatesRepository";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { UsersNamespacesRepository } from "../../app/persistence/UsersNamespacesRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { UserParticipationInNamespace } from "../../app/persistence/sharedTypes/UserInNamespaceModel";


describe(NamespacesAggregatesRepository.name, () => {
    let repository: NamespacesAggregatesRepository;
    let namespacesRepository: NamespacesRepository;
    let usersNamespacesRepository: UsersNamespacesRepository;

    beforeEach(() => {
        namespacesRepository = mock(NamespacesRepository)
        usersNamespacesRepository = mock(UsersNamespacesRepository)

        container.clearInstances();
        container.registerInstance(NamespacesRepository, instance(namespacesRepository))
        container.registerInstance(UsersNamespacesRepository, instance(usersNamespacesRepository))

        repository = container.resolve(NamespacesAggregatesRepository);
    });

    const namespace = {
        creator: { id: "creator_id" },
        id: "namespace_id",
        name: "namespace_name"
    } as Namespace;

    const admin: UserParticipationInNamespace = {
        namespace: { id: namespace.id },
        user: { id: "user_admin" },
        role: "admin",
        displayName: "user_admin_displayname"
    }

    it("should create a proper aggregate", async () => {

        when(namespacesRepository.getNamespaceById(namespace.id)).thenReturn(Promise.resolve(namespace))
        when(usersNamespacesRepository.getNamespaceUsers(deepEqual({ id: namespace.id }))).thenReturn(Promise.resolve([admin]))


        const aggregate = (await repository.getById({ id: "namespace_id" }))!
        const state = {
            namespace: namespace,
            usersAndRoles: new Map<string, UserManagement.UserParticipation>([[
                admin.user.id, { id: admin.user.id, role: admin.role, displayName: admin.displayName }
            ]]),
        } as UserManagement.NamespaceAggregateProps

        verify(namespacesRepository.getNamespaceById(namespace.id)).once()
        verify(usersNamespacesRepository.getNamespaceUsers(deepEqual({ id: namespace.id }))).once()

        expect(aggregate.props()).deep.equal({
            initial: state,
            current: state,
        })
    })

    it("should reflect role changes", async () => {
        const user1Participation: UserParticipationInNamespace = {
            namespace: { id: namespace.id },
            user: { id: "user_admin" },
            role: "admin",
            displayName: "admin_name"
        }

        const user2Participation: UserParticipationInNamespace = {
            namespace: { id: namespace.id },
            user: { id: "user_2" },
            role: "participant",
            displayName: "user_2_name"
        }


        when(namespacesRepository.getNamespaceById(namespace.id)).thenReturn(Promise.resolve(namespace))
        when(usersNamespacesRepository.getNamespaceUsers(deepEqual({ id: namespace.id })))
            .thenReturn(Promise.resolve([user1Participation, user2Participation]))

        const aggregate = (await repository.getById({ id: namespace.id }))!

        aggregate.modifyUserParticipation({ id: "user_admin", role: "participant", displayName: "no_longer_admin" })
        aggregate.addUserParticipation({ id: "user_3", role: "admin", displayName: "user_3_name" })
        aggregate.removeUser({ id: "user_2" })

        await repository.save(aggregate)

        verify(namespacesRepository.getNamespaceById(namespace.id)).once()
        verify(usersNamespacesRepository.getNamespaceUsers(deepEqual({ id: namespace.id }))).once()

        const [savedNamespacePairs] = capture(usersNamespacesRepository.generateActions).first()

        expect(savedNamespacePairs).to.deep.equal({
            added: [{
                namespace: { id: namespace.id },
                user: { id: "user_3" },
                role: "admin",
                displayName: "user_3_name",
            }],
            removed: [{
                namespace: { id: namespace.id },
                user: { id: "user_2" },
                role: "participant",
                displayName: "user_2_name"
            }],
            modified: [{
                namespace: { id: namespace.id },
                user: { id: "user_admin" },
                role: "participant",
                displayName: "no_longer_admin"
            }]
        } as Parameters<typeof UsersNamespacesRepository.prototype.generateActions>[0])
    })
})