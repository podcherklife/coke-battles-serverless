import { DynamoDBDocument, GetCommandInput, TransactWriteCommandInput } from "@aws-sdk/lib-dynamodb";
import { expect } from "chai";
import { anything, capture, instance, mock, when } from "ts-mockito";
import { container } from "tsyringe";
import { DynamoDbProperties, DynamoDbPropertiesInjectionToken } from "../../app/persistence/DynamoDbProperties";
import { NamespacesRepository } from "../../app/persistence/NamespacesRepository";
import { Namespace } from "../../app/persistence/sharedTypes/Namespace";
import { mockDynamoGetResult, mockDynamoTransactWriteResult } from "../mocks/dynamodbMocks";


describe(NamespacesRepository.name, () => {
    let dynamoDbMock: DynamoDBDocument;
    let repository: NamespacesRepository;
    let tableName = "table1";

    beforeEach(() => {
        dynamoDbMock = mock(DynamoDBDocument);
        container.clearInstances();
        container.registerInstance(DynamoDBDocument as any, instance(dynamoDbMock))
        container.registerInstance(DynamoDbPropertiesInjectionToken, {
            mainTableName: tableName
        } as DynamoDbProperties)

        repository = container.resolve(NamespacesRepository);
    });

    const sampleNamespace: Namespace = {
        id: "foo",
        name: "bar",
        creator: {
            id: "creator"
        }
    }

    describe(NamespacesRepository.prototype.getNamespaceById.name, () => {
        it("should generate a proper input and return item from dynamodb", async () => {
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(sampleNamespace));

            const result = await repository.getNamespaceById(sampleNamespace.id);

            expect(result).to.deep.equal(sampleNamespace);
            expect(capture(dynamoDbMock.get).first()[0]).to.deep.equal({
                TableName: tableName,
                Key: {
                    pk: "namespace",
                    sk: `namespace#${sampleNamespace.id}`
                }
            } as GetCommandInput)
        });
    });
    describe(NamespacesRepository.prototype.saveNamespace.name, () => {
        it("should generate a proper input if item does not exist", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(null));

            await repository.saveNamespace(sampleNamespace);

            expect(capture(dynamoDbMock.transactWrite).first()[0]).to.deep.equal({
                TransactItems: [{
                    Put: {
                        TableName: tableName,
                        Item: {
                            ...sampleNamespace,
                            pk: "namespace",
                            sk: `namespace#${sampleNamespace.id}`,
                            lsi1: `namespace_by_name#${sampleNamespace.name}`,
                            entityType: "namespace"
                        },
                        ConditionExpression: "attribute_not_exists(pk)"
                    }
                }, {
                    Put: {
                        TableName: tableName,
                        Item: {
                            pk: `namespace_name_unique#${sampleNamespace.name}`,
                            sk: "namespace_name_unique_sk",
                            entityName: "namespace_name_unique",
                        },
                        ConditionExpression: "attribute_not_exists(pk)"
                    }
                }]
            } as TransactWriteCommandInput)
        });
    });
    describe(NamespacesRepository.prototype.saveNamespace.name, () => {
        it("should generate a proper input if item already exists", async () => {
            when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
            let existingNamespaceState = { ...sampleNamespace, name: "newName" };
            when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(existingNamespaceState));

            await repository.saveNamespace(sampleNamespace);

            expect(capture(dynamoDbMock.transactWrite).first()[0]).to.deep.equal({
                TransactItems: [{
                    Put: {
                        TableName: tableName,
                        Item: {
                            ...sampleNamespace,
                            pk: "namespace",
                            sk: `namespace#${sampleNamespace.id}`,
                            lsi1: `namespace_by_name#${sampleNamespace.name}`,
                            entityType: "namespace"
                        },
                        ConditionExpression: "attribute_exists(pk)"
                    }
                }, {
                    Put: {
                        TableName: tableName,
                        Item: {
                            pk: `namespace_name_unique#${sampleNamespace.name}`,
                            sk: "namespace_name_unique_sk",
                            entityName: "namespace_name_unique",
                        },
                        ConditionExpression: "attribute_not_exists(pk)"
                    }
                }, {
                    Delete: {
                        TableName: tableName,
                        Key: {
                            pk: `namespace_name_unique#${existingNamespaceState.name}`,
                            sk: "namespace_name_unique_sk",
                        },
                        ConditionExpression: "attribute_exists(pk)"
                    }
                }]
            } as TransactWriteCommandInput)
        });
    });
});

