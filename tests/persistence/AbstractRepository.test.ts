import { BatchStatementErrorCodeEnum, TransactionCanceledException } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { expect } from "chai";
import { anything, capture, instance, mock, when } from "ts-mockito";
import { AbstractRepository, ConditionCheckError, UniquenessItem } from "../../app/persistence/AbstractRepository";
import { mockDynamoGetResult, mockDynamoTransactWriteResult } from "../mocks/dynamodbMocks";

interface SampleEntity {
    id: string,
    sk: string,
    prop1: string,
    prop2: string
}

let tableName = "sampleTable";
let entityName = "sampleEntity";
class RepositoryImpl extends AbstractRepository<SampleEntity> {
    constructor(dynamoDb: DynamoDBDocument) {
        super(entityName, dynamoDb, tableName);
    }
    generatePkProperties(item: SampleEntity) {
        return {
            pk: item.id,
            sk: item.sk
        };
    }

    generateUniquenessItems(item: SampleEntity): UniquenessItem[] {
        return [{
            entityType: "prop1_unique_entity",
            tableName: tableName,
            key: {
                pk: item.prop1,
                sk: "prop1_unique_sk"
            }
        },
        {
            entityType: "prop2_unique_entity",
            tableName: tableName,
            key: {
                pk: item.prop2,
                sk: "prop2_unique_sk"
            }
        }];
    }

    stripProperties(item: SampleEntity) {
        return { id: item.id, sk: item.sk, prop1: item.prop1, prop2: item.prop2 };
    }

    save(item: SampleEntity) {
        return super.save(item);
    }
}

describe(AbstractRepository.name, () => {
    let dynamoDbMock: DynamoDBDocument;
    let repository: RepositoryImpl;

    beforeEach(() => {
        dynamoDbMock = mock(DynamoDBDocument);
        repository = new RepositoryImpl(instance(dynamoDbMock));
    });


    let sampleEntity: SampleEntity = {
        id: "id",
        sk: "sk",
        prop1: "prop1",
        prop2: "prop2"
    }

    it("Should generate uniqueness items if item does not exist", async () => {
        when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(null));
        when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
        await repository.save(sampleEntity);
        let firstCall = capture(dynamoDbMock.transactWrite).first()
        expect(firstCall[0]).to.deep.equal({
            TransactItems: [{
                Put: {
                    TableName: tableName,
                    Item: {
                        ...sampleEntity,
                        pk: sampleEntity.id,
                        sk: sampleEntity.sk,
                        entityType: entityName
                    },
                    ConditionExpression: "attribute_not_exists(pk)"
                }
            }, {
                Put: {
                    TableName: tableName,
                    Item: {
                        pk: sampleEntity.prop1,
                        sk: "prop1_unique_sk",
                        entityName: "prop1_unique_entity",
                    },
                    ConditionExpression: "attribute_not_exists(pk)"
                }
            },
            {
                Put: {
                    TableName: tableName,
                    Item: {
                        pk: sampleEntity.prop2,
                        sk: "prop2_unique_sk",
                        entityName: "prop2_unique_entity",
                    },
                    ConditionExpression: "attribute_not_exists(pk)"
                }
            }]
        });
    });


    it("should throw transaction exception on ConditionalCheckFailed", async () => {
        let error = {
            CancellationReasons: [
                {
                    Code: BatchStatementErrorCodeEnum.ConditionalCheckFailed
                }
            ]
        } as TransactionCanceledException;
        when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(sampleEntity));
        when(dynamoDbMock.transactWrite(anything())).thenReturn(Promise.reject(error));

        try {
            await repository.save(sampleEntity);
        } catch (e) {
            expect(e).to.be.an.instanceOf(ConditionCheckError);
        }

    });

    it("Should update uniqueness items if item already exists", async () => {
        let initialState = sampleEntity;
        let updatedState = { ...sampleEntity, prop2: "updatedProp2" };
        when(dynamoDbMock.get(anything())).thenReturn(mockDynamoGetResult(initialState));
        when(dynamoDbMock.transactWrite(anything())).thenReturn(mockDynamoTransactWriteResult());
        await repository.save(updatedState);

        expect(capture(dynamoDbMock.transactWrite).first()[0]).to.deep.equal({
            TransactItems: [{
                Put: {
                    TableName: tableName,
                    Item: {
                        ...updatedState,
                        pk: updatedState.id,
                        sk: updatedState.sk,
                        entityType: entityName
                    },
                    ConditionExpression: "attribute_exists(pk)"
                }
            },
            {
                Put: {
                    TableName: tableName,
                    Item: {
                        pk: updatedState.prop2,
                        sk: "prop2_unique_sk",
                        entityName: "prop2_unique_entity",
                    },
                    ConditionExpression: "attribute_not_exists(pk)"
                }
            }, {
                Delete: {
                    TableName: tableName,
                    Key: {
                        pk: initialState.prop2,
                        sk: "prop2_unique_sk",
                    },
                    ConditionExpression: "attribute_exists(pk)"
                }
            }]
        });
    });
});