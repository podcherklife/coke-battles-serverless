import { when } from "ts-mockito";
import { NamespacesRepository } from "../app/persistence/NamespacesRepository";
import { Namespace } from "../app/persistence/sharedTypes/Namespace";

export function withNamespace(namespace: Namespace, repository: NamespacesRepository) {
    when(repository.getNamespaceById(namespace.id)).thenReturn(Promise.resolve(namespace));
}

